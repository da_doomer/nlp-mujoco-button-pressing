# Backend

**INPUT: "Go to the third button"**

![Video](mujoco_np/example_scripts/compound_instruction.webm)

## How to run

*Note: experiments were run on a 256-cpu server.*

Clone the repo and pull submodules:

```sh
git clone [repo URL]
git submodule update --init --recursive
cd [repo dir]/backend
```

Export the `OPENAI_API_KEY` variable with your OpenAI API key. In bash
it would be done like this:

```
$ export OPENAI_API_KEY=[your API key]
```

If you want to run the interaction loop you need to create a Google Firebase
project, then add the Google Firebase credentials to
`mujoco_np/messages/credentials.json`. The file should look something like
this:

```json
{
  "type": "...",
  "project_id": "...",
  "private_key_id": "...",
  "private_key": "...",
  "client_email": "...",
  "client_id": "...",
  "auth_uri": "...",
  "token_uri": "...",
  "auth_provider_x509_cert_url": "...",
  "client_x509_cert_url": "...",
  "universe_domain": "..."
}
```

### Test script

Then run the test script:

```sh
# Install dependencies
# Option 1) If you have poetry:
poetry install
poetry shell
pip install -e lib/ant_crafter

# Option 2) Use Python's built-in venv module.
# Assuming you are running bash:
python -m venv venv
source venv/bin/activate
pip install -e lib/* .

# Adjust the number of workers
python mujoco_np/example_scripts/compound_instruction.py --map mujoco_np/example_map.json --max_workers 16 --output_dir temp
```

**NOTE:** depending on your environment, you might have to run python under
`xvfb-run`. For example:

```sh
xvfb-run --auto-servernum python mujoco_np/example_scripts/compound_instruction.py --map mujoco_np/example_map.json --max_workers 16 --output_dir temp
```

### Interaction loops

Generate a sequence of tasks:

```sh
python lib/ant-crafter/ant_crafter/task_sampling.py --seed "asd" --task_n 3 --item_sequence_size 2 --output_dir output_results/task_dir
```

Then execute the interaction loop:

```sh
xvfb-run --auto-servernum python mujoco_np/interaction_loop/main.py \
    --session_parameters_json mujoco_np/interaction_loop/session_parameters.json \
    --seed "asd" \
    --task_dir output_results/task_dir \
    --chain_id $(date)
```

*Note: on first run, Firebase will fail and say that 'the query requires an
index'. The error message will contain a link. Click on that link,
then click on 'save' to automatically create the required index.*
