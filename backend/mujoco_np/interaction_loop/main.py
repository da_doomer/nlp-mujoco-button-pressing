"""Driving program for the interaction loop."""
from pathlib import Path
from ant_crafter.planning import PlannerOptions
from ant_crafter.mujoco.simulation import get_simulation_instance
from ant_crafter.task_sampling import Task
from mujoco_np.messages.firebase import get_chain_ids, store_chain_id
from mujoco_np.initial_libraries import get_initial_predicate_grounding_library
from mujoco_np.initial_libraries import get_initial_steps_library
from mujoco_np.interaction_loop.session_types import NPLibrary
from mujoco_np.interaction_loop.session_types import SolverParameters
from mujoco_np.interaction_loop.session_types import InitialSession
from mujoco_np.interaction_loop.loop import run_loop
import argparse
import json

from mujoco_np.solution_database.nearest_neighbor import warmup_cache


def main(
    llm_user: bool,
    task_dir: Path,
    solver_parameters: SolverParameters,
    response_timeout_s: float,
    chain_id: str,
    st_model: str,  # SentenceTransformers model
    plan_database: Path,
    device: str,
    seed: str,
):
    # Make sure chain ID is new
    chain_ids = get_chain_ids()
    if chain_id in chain_ids:
        raise ValueError(f"Chain ID '{chain_id}' already exists!")

    # Push chain ID
    store_chain_id(chain_id)
    print(f"Stored chain ID '{chain_id}'")

    task_dirs_json = task_dir/"task_dirs.json"
    with open(task_dirs_json, "rt") as fp:
        task_dirs: list[str] = json.load(fp)

    # Initialize empty library
    predicate_grounding_library = get_initial_predicate_grounding_library()
    decomposition_library = get_initial_steps_library()
    library = NPLibrary(
        predicate_grounding_library=predicate_grounding_library,
        decomposition_library=decomposition_library,
    )

    # Warmup planning cache
    warmup_cache(
        model=st_model,
        device=device,
        solution_database_file=plan_database,
    )

    # Go through each task
    for i, tdir in enumerate(task_dirs):
        print(tdir)
        task = Task.from_path(task_dir/tdir/"task.json")

        # Obtain initial state
        current_state = get_simulation_instance(
            map_spec=task.map_spec,
            animation_fps=0.0,
            sub_step_s=solver_parameters.sub_step_s,
        ).state

        # Assemble initial session
        session = InitialSession(
            task=task,
            current_state=current_state,
            library=library,
            solver_parameters=solver_parameters,
            plan_so_far=list(),
            chain_id=chain_id,
            llm_user=llm_user,
            st_model=st_model,
            plan_database=plan_database,
            device=device,
            seed=seed,
        )

        # Loop until task is done
        run_loop(
            session=session,
            response_timeout_s=response_timeout_s,
        )

        print(f"Task {i+1}/{len(tasks)} done!")
    print(f"All tasks complete!")


if __name__ == "__main__":
    parser = argparse.ArgumentParser(
        prog="Interaction loop main",
        description='Run the interaction loop on a sequence of tasks.',
    )
    _ = parser.add_argument(
        '--task_dir',
        type=Path,
        required=True,
        help='Directory with task sub-directories.',
    )
    _ = parser.add_argument(
        '--llm_user',
        action="store_true",
        help='Use LLM "fake" user.',
    )
    _ = parser.add_argument(
        '--seed',
        type=str,
        required=True,
        help='Random seed.',
    )
    _ = parser.add_argument(
        '--chain_id',
        type=str,
        required=True,
        help='Chain ID.',
    )
    _ = parser.add_argument(
        '--session_parameters_json',
        type=Path,
        required=True,
        help='Solver parameters JSON file.',
    )
    _ = parser.add_argument(
        '--st_model',
        type=str,
        required=True,
        help="""
        SentenceTransformers model for nearest neighbors lookups.
        It must match the model used to populate the nearest neighbors
        database.
        """,
    )
    _ = parser.add_argument(
        '--device',
        type=str,
        required=True,
        help="CUDA device for nearest neighbors lookups.",
    )
    _ = parser.add_argument(
        '--plan_database',
        type=Path,
        required=True,
        help='Database file with solved planning problems.',
    )
    args = parser.parse_args()

    # Parse solver parameters JSON
    with open(args.session_parameters_json, "rt") as fp:
        session_parameters = json.load(fp)
    _solver_parameters = session_parameters["solver_parameters"]
    planner_options = PlannerOptions(
        **_solver_parameters["planner_options"]
    )
    solver_parameters = SolverParameters(
        solver_max_iter_n=_solver_parameters["solver_max_iter_n"],
        sub_step_s=_solver_parameters["sub_step_s"],
        max_workers=_solver_parameters["max_workers"],
        llm_attempt_n=_solver_parameters["llm_attempt_n"],
        planner_options=planner_options,
    )

    main(
        llm_user=args.llm_user,
        task_dir=args.task_dir,
        solver_parameters=solver_parameters,
        response_timeout_s=session_parameters["response_timeout_s"],
        chain_id=args.chain_id,
        st_model=args.st_model,
        plan_database=args.plan_database,
        device=args.device,
        seed=args.seed,
    )
