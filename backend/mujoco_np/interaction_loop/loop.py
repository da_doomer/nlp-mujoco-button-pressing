"""Interaction loop."""
from mujoco_np.interaction_loop.sessions import run_session
from mujoco_np.interaction_loop.session_types import Done
from mujoco_np.interaction_loop.session_types import Session
from mujoco_np.messages.firebase import store_session_outcome
from mujoco_np.messages.firebase import store_session
from mujoco_np.messages.firebase import NO_PREVIOUS_SESSION


def run_loop(
    session: Session,
    response_timeout_s: float,
):
    """Loop sessions until `Done` is achieved."""
    next_session = session
    previous_session_id = NO_PREVIOUS_SESSION
    while not isinstance(next_session, Done):
        # Store session
        session_id = store_session(session=next_session)
        print(f"Stored current session; ID: '{session_id}'")

        # Execute session
        outcome = run_session(
            session=next_session,
            response_timeout_s=response_timeout_s,
        )

        # Store outcome
        _ = store_session_outcome(
            session_id=session_id,
            previous_session_id=previous_session_id,
            interaction_prompt_id=outcome.stored_interaction.prompt_id,
            interaction_response_id=outcome.stored_interaction.response_id,
            next_session=outcome.next_session,
        )

        # Prepare to run next session
        previous_session_id = session_id
        next_session = outcome.next_session
