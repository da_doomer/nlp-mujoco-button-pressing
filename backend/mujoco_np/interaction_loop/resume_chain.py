"""Load the latest session in a chain and continue the interaction loop."""
from mujoco_np.messages.firebase import get_session
from mujoco_np.messages.firebase import get_latest_session_id
from mujoco_np.interaction_loop.loop import run_loop
import argparse


def main(
    chain_id: str,
    response_timeout_s: float,
):
    # Get latest session
    session_id = get_latest_session_id(chain_id)

    # Get session from database
    session = get_session(session_id)

    # Loop until task is done
    run_loop(
        session=session,
        response_timeout_s=response_timeout_s,
    )


if __name__ == "__main__":
    parser = argparse.ArgumentParser(
        prog="Resume chain",
        description='Load the latest session in a given chain and continue interaction loop.',
    )
    parser.add_argument(
        '--chain_id',
        type=str,
        required=True,
        help='Session ID stored in Firebase.',
    )
    parser.add_argument(
        '--response_timeout_s',
        type=float,
        required=True,
        help='Session ID stored in Firebase.',
    )
    args = parser.parse_args()

    # Parse solver parameters JSON

    main(
        chain_id=args.chain_id,
        response_timeout_s=args.response_timeout_s,
    )
