"""Parse a string representation of a boolean value."""


def parse_bool_response(response: str) -> bool:
    """Parse a string representation of a boolean value. This is a simple
    hacky and scrappy, non-complete, heuristic use-at-your-own-peril check."""
    # If any of these strings is a substring of the input, then return True.
    # Otherwise, return False.
    affirmative_responses = [
        "yes",
        "affirmative",
        "indeed",
        "true",
        "yessir",
        "ok",
        "if you say so",
    ]
    if any([yes in response.lower() for yes in affirmative_responses]):
        return True
    return False
