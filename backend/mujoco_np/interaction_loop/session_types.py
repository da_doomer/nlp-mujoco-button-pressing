"""Type definitions in the NP interaction loop."""
from dataclasses import dataclass
from ant_crafter.planning import PlannerOptions
from ant_crafter.task_sampling import Task
from ant_crafter.predicates import Predicate
from ant_crafter.mujoco.simulation import SimulationState
from ant_crafter.mujoco.simulation import get_simulation_instance
from mujoco_np.natural_programs.types import PredicateGroundingDB
from mujoco_np.natural_programs.types import NaturalProgramSolved
from mujoco_np.natural_programs.types import NaturalProgram
from mujoco_np.natural_programs.types import DecompositionDB
from mujoco_np.natural_programs.solver import get_flattened_solved_np
from pathlib import Path
from PIL import Image
import numpy as np
import json


Plan = list[
    NaturalProgramSolved[Predicate, list[float],]
]


@dataclass
class NPLibrary:
    predicate_grounding_library: PredicateGroundingDB
    decomposition_library: DecompositionDB


@dataclass
class SolverParameters:
    solver_max_iter_n: int
    sub_step_s: float
    max_workers: int
    planner_options: PlannerOptions
    llm_attempt_n: int


@dataclass
class Session:
    task: Task
    current_state: SimulationState
    library: NPLibrary
    solver_parameters: SolverParameters
    plan_so_far: Plan
    chain_id: str
    llm_user: bool
    st_model: str  # SentenceTransformers model
    plan_database: Path
    device: str
    seed: str

    def save_current_state_image(self, path: Path):
        """A path to the current state."""
        sim = get_simulation_instance(
            map_spec=self.task.map_spec,
            animation_fps=60.0,
            sub_step_s=self.solver_parameters.sub_step_s,
        )

        for step in self.plan_so_far:
            step_actions = get_flattened_solved_np(step)
            for action in step_actions:
                sim.step(np.array(action))

        pixels = sim.current_image
        im = Image.fromarray(pixels)
        im.save(path)

    def save_actions_video(self, path: Path, actions: list[list[float]]):
        sim = get_simulation_instance(
            map_spec=self.task.map_spec,
            animation_fps=60.0,
            sub_step_s=self.solver_parameters.sub_step_s,
        )

        # Execute actions
        for action in actions:
            sim.step(np.array(action))

        # Save video
        sim.save_video(path)

    def save_plan_so_far_video(self, path: Path):
        """A path to a video of the plan so far."""
        # Flatten plan
        actions = list[list[float]]()
        for step in self.plan_so_far:
            step_actions = get_flattened_solved_np(step)
            for action in step_actions:
                actions.append(action)

        # Save video
        self.save_actions_video(path, actions)

    def save_full_video(self, path: Path):
        """Save the full video: plan so far and any plan that is not yet
        confirmed."""
        self.save_plan_so_far_video(path)


@dataclass
class InitialSession(Session):
    pass


@dataclass
class EvaluateSession(Session):
    latest_command: str
    latest_command_plan_candidate: Plan
    latest_command_np: NaturalProgram[Predicate]
    plan_before_command: Plan

    def save_latest_command_plan_video(self, path: Path):
        """A path to a video of the latest command plan."""
        sim = get_simulation_instance(
            map_spec=self.task.map_spec,
            animation_fps=60.0,
            sub_step_s=self.solver_parameters.sub_step_s,
        )

        # Fast-forward plan so far
        for step in self.plan_so_far:
            step_actions = get_flattened_solved_np(step)
            for action in step_actions:
                sim.step(np.array(action))

        sim.clear_video()

        # Step latest command plan candidate
        for step in self.latest_command_plan_candidate:
            step_actions = get_flattened_solved_np(step)
            for action in step_actions:
                sim.step(np.array(action))

        # Save video
        sim.save_video(path)

    def save_full_video(self, path: Path):
        """Save the full video: plan so far and any plan that is not yet
        confirmed."""
        # Flatten plan
        actions = list[list[float]]()
        for step in self.plan_so_far:
            step_actions = get_flattened_solved_np(step)
            for action in step_actions:
                actions.append(action)
        for step in self.latest_command_plan_candidate:
            step_actions = get_flattened_solved_np(step)
            for action in step_actions:
                actions.append(action)
        self.save_actions_video(path, actions)


@dataclass
class FixSession(Session):
    failed_command: str
    failed_command_plan_candidate: Plan

    def save_failed_command_plan_candidate_video(self, path: Path):
        """A path to a video of the latest command plan candidate."""
        sim = get_simulation_instance(
            map_spec=self.task.map_spec,
            animation_fps=60.0,
            sub_step_s=self.solver_parameters.sub_step_s,
        )

        # Fast-forward plan so far
        for step in self.plan_so_far:
            step_actions = get_flattened_solved_np(step)
            for action in step_actions:
                sim.step(np.array(action))

        sim.clear_video()

        # Step latest command plan candidate
        for step in self.failed_command_plan_candidate:
            step_actions = get_flattened_solved_np(step)
            for action in step_actions:
                sim.step(np.array(action))

        # Save video
        sim.save_video(path)

    def save_full_video(self, path: Path):
        """Save the full video: plan so far and any plan that is not yet
        confirmed."""
        # Flatten plan
        actions = list[list[float]]()
        for step in self.plan_so_far:
            step_actions = get_flattened_solved_np(step)
            for action in step_actions:
                actions.append(action)
        for step in self.failed_command_plan_candidate:
            step_actions = get_flattened_solved_np(step)
            for action in step_actions:
                actions.append(action)
        self.save_actions_video(path, actions)


@dataclass
class ExtendSession(Session):
    pass


@dataclass
class Done(Session):
    latest_command: str
