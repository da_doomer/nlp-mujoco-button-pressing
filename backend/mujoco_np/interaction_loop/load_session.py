"""Load a session and continue the interaction loop."""
from mujoco_np.messages.firebase import get_session
from mujoco_np.interaction_loop.loop import run_loop
import argparse


def main(
    session_id: str,
    response_timeout_s: float,
):
    # Get session from database
    session = get_session(session_id)

    # Loop until task is done
    run_loop(
        session=session,
        response_timeout_s=response_timeout_s,
    )


if __name__ == "__main__":
    parser = argparse.ArgumentParser(
        prog="Session loading",
        description='Run the interaction loop starting on a given session.',
    )
    parser.add_argument(
        '--session_id',
        type=str,
        required=True,
        help='Session ID stored in Firebase.',
    )
    parser.add_argument(
        '--response_timeout_s',
        type=float,
        required=True,
        help='Session ID stored in Firebase.',
    )
    args = parser.parse_args()

    # Parse solver parameters JSON

    main(
        session_id=args.session_id,
        response_timeout_s=args.response_timeout_s,
    )
