"""Operations on libraries."""

from mujoco_np.interaction_loop.session_types import NPLibrary
from mujoco_np.natural_programs.types import PredicateGroundingExample
from mujoco_np.natural_programs.types import NaturalLanguageString
from mujoco_np.natural_programs.types import Predicate
from mujoco_np.natural_programs.types import DecompositionExample
from mujoco_np.natural_programs.types import Action
from mujoco_np.natural_programs.types import NaturalProgramSolved


def get_predicate_groundings(
    solved_np: NaturalProgramSolved[Predicate, Action],
) -> list[PredicateGroundingExample]:
    """Recursively obtain all the instruction examples
    induced by the given solved NP."""
    # Assemble top-level instruction
    predicate_grounding = PredicateGroundingExample(
        instruction=solved_np.natural_program.instruction,
        predicate_generator_str=solved_np.natural_program.predicate_generator_str,
        map_spec=solved_np.map_spec,
        simulation_state=solved_np.simulation_state,
    )

    # Recursively obtain the instructions in the tree
    predicate_groundings = [predicate_grounding ]
    for step in solved_np.steps:
        if isinstance(step, NaturalProgramSolved):
            step_predicate_groundings= get_predicate_groundings(step)
            predicate_groundings.extend(step_predicate_groundings)
    return predicate_groundings


def get_decompositions(
    solved_np: NaturalProgramSolved[Predicate, Action],
) -> list[DecompositionExample]:
    """Recursively obtain all the instruction examples
    induced by the given solved NP."""
    # Obtain steps
    steps = list[NaturalLanguageString]()
    for step in solved_np.steps:
        if isinstance(step, NaturalProgramSolved):
            steps.append(step.natural_program.instruction)

    # Assemble top-level instruction
    decomposition = DecompositionExample(
        instruction=solved_np.natural_program.instruction,
        steps=steps,
        map_spec=solved_np.map_spec,
        simulation_state=solved_np.simulation_state,
    )

    # Recursively obtain the instructions in the tree
    decompositions = [decomposition]
    for step in solved_np.steps:
        if isinstance(step, NaturalProgramSolved):
            step_decompositions = get_decompositions(step)
            decompositions.extend(step_decompositions)
    return decompositions


def add_solved_np(
    solved_np: NaturalProgramSolved[Predicate, Action],
    library: NPLibrary,
) -> NPLibrary:
    """Add the given solved NP to the library."""
    # Extend instruction library
    new_predicate_grounding_library = list(library.predicate_grounding_library)
    for instruction in get_predicate_groundings(solved_np):
        new_predicate_grounding_library.append(instruction)

    # Extend decomposition library
    new_decomposition_library = list(library.decomposition_library)
    for decomposition in get_decompositions(solved_np):
        new_decomposition_library.append(decomposition)

    new_library = NPLibrary(
        predicate_grounding_library=new_predicate_grounding_library,
        decomposition_library=new_decomposition_library,
    )
    return new_library
