"""Instruction generation."""
from mujoco_np.interaction_loop.session_types import Task


def get_instructions(task: Task) -> list[str]:
    """Get the generic instructions for the task."""
    # Select crafting rules
    relevant_recipes = [
        recipe
        for recipe in task.map_spec.recipes
        if recipe.output_item in task.item_sequence
    ]

    # Generate string representation of crafting rules
    recipe_book_str = [
        f"{'+'.join(recipe.input_items)} produces {recipe.output_item}"
        for recipe in relevant_recipes
    ]

    # Generate string representation of items that have to be crafted
    item_str = ", ".join([
        item
        for item in task.item_sequence
    ])
    instructions = [
        "Hi there! Read carefully and submit your response at the bottom.",
        "",
        "The critter can move and push items around.",
        "",
        "The goal is to help the critter craft some items.",
        "These are crafting rules:",
        *recipe_book_str,
        "",
        "The following items should be crafted:",
        item_str,
        "Pushing items so they collide is the way items are crafted.",
        "Hint: usually to craft an item the robot would make contact with an item AND make an item-item-contact between that item and the other item.",
        "",
    ]
    return instructions
