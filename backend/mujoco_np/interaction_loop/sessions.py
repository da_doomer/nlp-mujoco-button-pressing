"""Sessions are sequences of interaction steps between the user and the
system."""
from ant_crafter.mujoco.simulation import SimulationState
from ant_crafter.mujoco.simulation import get_simulation_instance
from ant_crafter.mujoco.simulation import get_abstract_map_state
from ant_crafter.mujoco.map import MapSpec
from ant_crafter.predicates import Predicate
from ant_crafter.task_sampling import Task
from mujoco_np.interaction_loop.library import add_solved_np
from mujoco_np.interaction_loop.session_types import InitialSession
from mujoco_np.interaction_loop.session_types import EvaluateSession
from mujoco_np.interaction_loop.session_types import FixSession
from mujoco_np.interaction_loop.session_types import ExtendSession
from mujoco_np.interaction_loop.session_types import Done
from mujoco_np.interaction_loop.session_types import Session
from mujoco_np.interaction_loop.parse_bool_response import parse_bool_response
from mujoco_np.messages.firebase import StoredInteraction
from mujoco_np.messages.user import ask_user, get_extend_prompt
from mujoco_np.messages.user import get_evaluate_prompt
from mujoco_np.messages.user import get_fix_prompt
from mujoco_np.messages.user import get_initial_prompt
from mujoco_np.messages.types import Response
from mujoco_np.natural_programs.solver import solve
from mujoco_np.natural_programs.solver import instruction_to_np
from mujoco_np.natural_programs.solver import get_flattened_solved_np
from mujoco_np.natural_programs.types import NaturalLanguageString
from mujoco_np.natural_programs.types import NaturalProgramSolved
import random
from dataclasses import dataclass
import numpy

from mujoco_np.solution_database.solutions import SolvedPlanningProblem
from mujoco_np.solution_database.solutions import save_solved_planning_problem


@dataclass
class SessionOutcome:
    next_session: Session | Done
    stored_interaction: StoredInteraction


def run_session(
    session: Session,
    response_timeout_s: float,
) -> SessionOutcome:
    if isinstance(session, InitialSession):
        return run_initial_session(
            session=session,
            response_timeout_s=response_timeout_s,
        )
    elif isinstance(session, ExtendSession):
        return run_extend_session(
            session=session,
            response_timeout_s=response_timeout_s,
        )
    elif isinstance(session, FixSession):
        return run_fix_session(
            session=session,
            response_timeout_s=response_timeout_s,
        )
    elif isinstance(session, EvaluateSession):
        return run_evaluate_session(
            session=session,
            response_timeout_s=response_timeout_s,
        )
    else:
        raise TypeError(f"Session of unrecognized type '{type(session)}'!")


def run_initial_session(
    session: InitialSession,
    response_timeout_s: float
) -> SessionOutcome:
    prompt = get_initial_prompt(
        session,
        llm_user=session.llm_user,
    )
    stored_interaction = ask_user(
        prompt,
        chain_id=session.chain_id,
        timeout_s=response_timeout_s,
        llm_user=session.llm_user,
    )
    next_session = run_and_process_command(
        session=session,
        command=stored_interaction.response,
    )
    session_outcome = SessionOutcome(
        next_session=next_session,
        stored_interaction=stored_interaction,
    )
    return session_outcome


def get_current_state(
    map_spec: MapSpec,
    plan_so_far: list[NaturalProgramSolved[Predicate, list[float]]],
    sub_step_s: float,
) -> SimulationState:
    """Return the state after executing the given plan."""
    sim = get_simulation_instance(
        map_spec=map_spec,
        animation_fps=None,
        sub_step_s=sub_step_s,
    )
    actions = list[list[float]]()
    for step in plan_so_far:
        step_actions = get_flattened_solved_np(step)
        actions.extend(step_actions)
    for action in actions:
        sim.step(numpy.array(action))
    return sim.state


def run_evaluate_session(
    session: EvaluateSession,
    response_timeout_s: float
) -> SessionOutcome:
    prompt = get_evaluate_prompt(
        session,
        llm_user=session.llm_user,
    )
    stored_interaction = ask_user(
        prompt,
        chain_id=session.chain_id,
        timeout_s=response_timeout_s,
        llm_user=session.llm_user,
    )
    success = parse_bool_response(stored_interaction.response.data)

    _random = random.Random(session.seed)
    if success:
        # Extend library
        previous_state = get_current_state(
            session.task.map_spec,
            session.plan_before_command,
            sub_step_s=session.solver_parameters.sub_step_s,
        )
        solved_np = NaturalProgramSolved(
            natural_program=session.latest_command_np,
            steps=session.latest_command_plan_candidate,
            map_spec=session.task.map_spec,
            simulation_state=previous_state,
        )
        new_library = add_solved_np(
            solved_np=solved_np,
            library=session.library,
        )

        # Save plan to plan database
        plan_so_far = session.plan_before_command \
            + session.latest_command_plan_candidate
        current_state = get_current_state(
            session.task.map_spec,
            plan_so_far,
            sub_step_s=session.solver_parameters.sub_step_s,
        )
        plan = list[list[float]]()
        for np in session.latest_command_plan_candidate:
            actions = get_flattened_solved_np(np)
            plan.extend(actions)
        solution = SolvedPlanningProblem(
            instruction=session.latest_command_np.instruction,
            predicate=session.latest_command_np.predicate,
            state=current_state,
            map_spec=session.task.map_spec,
            plan=plan,
        )
        key = save_solved_planning_problem(
            solution=solution,
            database_file=session.plan_database,
        )
        print(f"Saved plan! Key: '{key}'")

        # Execute plan to get new current state
        next_session = ExtendSession(
            plan_so_far=plan_so_far,
            task=session.task,
            current_state=current_state,
            library=new_library,
            solver_parameters=session.solver_parameters,
            chain_id=session.chain_id,
            llm_user=session.llm_user,
            st_model=session.st_model,
            plan_database=session.plan_database,
            device=session.device,
            seed=str(_random.random()),
        )
        session_outcome = SessionOutcome(
            next_session=next_session,
            stored_interaction=stored_interaction,
        )
        return session_outcome

    next_session = FixSession(
        failed_command=session.latest_command,
        failed_command_plan_candidate=session.latest_command_plan_candidate,
        task=session.task,
        current_state=session.current_state,
        library=session.library,
        solver_parameters=session.solver_parameters,
        plan_so_far=session.plan_so_far,
        chain_id=session.chain_id,
        llm_user=session.llm_user,
        st_model=session.st_model,
        plan_database=session.plan_database,
        device=session.device,
        seed=str(_random.random()),
    )
    session_outcome = SessionOutcome(
        next_session=next_session,
        stored_interaction=stored_interaction,
    )
    return session_outcome


def run_fix_session(
    session: FixSession,
    response_timeout_s: float
) -> SessionOutcome:
    prompt = get_fix_prompt(
        session,
        llm_user=session.llm_user,
    )
    stored_interaction = ask_user(
        prompt,
        chain_id=session.chain_id,
        timeout_s=response_timeout_s,
        llm_user=session.llm_user,
    )
    next_session = run_and_process_command(
        command=stored_interaction.response,
        session=session,
    )
    session_outcome = SessionOutcome(
        next_session=next_session,
        stored_interaction=stored_interaction,
    )
    return session_outcome


def run_extend_session(
    session: ExtendSession,
    response_timeout_s: float
) -> SessionOutcome:
    prompt = get_extend_prompt(
        session=session,
        llm_user=session.llm_user,
    )
    stored_interaction = ask_user(
        prompt,
        chain_id=session.chain_id,
        timeout_s=response_timeout_s,
        llm_user=session.llm_user,
    )
    next_session = run_and_process_command(
        command=stored_interaction .response,
        session=session,
    )
    session_outcome = SessionOutcome(
        next_session=next_session,
        stored_interaction=stored_interaction,
    )
    return session_outcome


def is_solved(
    task: Task,
    plan: list[NaturalProgramSolved[Predicate, list[float]]],
    sub_step_s: float,
) -> bool:
    """Returns whether the plan solves the given task."""
    current_state = get_current_state(
        map_spec=task.map_spec,
        plan_so_far=plan,
        sub_step_s=sub_step_s,
    )
    success = all([
        item.item_name in current_state.task_state.crafted_items_log
        for item in task.map_spec.items
    ])
    return success


def run_and_process_command(
    command: Response,
    session: Session,
) -> FixSession | EvaluateSession | Done:
    _random = random.Random(session.seed)
    sim = get_simulation_instance(
        map_spec=session.task.map_spec,
        animation_fps=None,
        sub_step_s=session.solver_parameters.sub_step_s,
    )
    sim.state = session.current_state
    abstract_state = get_abstract_map_state(sim)
    np = instruction_to_np(
        instruction=NaturalLanguageString(command.data),
        abstract_state=abstract_state,
        predicate_grounding_library=session.library.predicate_grounding_library,
        llm_attempt_n=session.solver_parameters.llm_attempt_n,
    )
    plan = solve(
        np=np,
        decomposition_library=session.library.decomposition_library,
        predicate_grounding_library=session.library.predicate_grounding_library,
        map_spec=session.task.map_spec,
        simulation_state=session.current_state,
        solver_max_iter_n=session.solver_parameters.solver_max_iter_n,
        sub_step_s=session.solver_parameters.sub_step_s,
        max_workers=session.solver_parameters.max_workers,
        planner_options=session.solver_parameters.planner_options,
        seed=str(_random.random()),
        st_model=session.st_model,
        plan_database=session.plan_database,
        device=session.device,
        verbose=True,
    )

    if not plan.is_sat:
        return FixSession(
            failed_command=command.data,
            failed_command_plan_candidate=[plan.plan],
            task=session.task,
            current_state=session.current_state,
            library=session.library,
            solver_parameters=session.solver_parameters,
            plan_so_far=session.plan_so_far,
            chain_id=session.chain_id,
            llm_user=session.llm_user,
            st_model=session.st_model,
            plan_database=session.plan_database,
            device=session.device,
            seed=str(_random.random()),
        )

    plan_so_far = session.plan_so_far \
        + [plan.plan]

    success = is_solved(
        plan=plan_so_far,
        task=session.task,
        sub_step_s=session.solver_parameters.sub_step_s
    )

    current_state = get_current_state(
        session.task.map_spec,
        plan_so_far,
        sub_step_s=session.solver_parameters.sub_step_s,
    )

    if success:
        # Extend library
        solved_np = plan.plan
        new_library = add_solved_np(
            solved_np=solved_np,
            library=session.library,
        )
        return Done(
            task=session.task,
            current_state=current_state,
            latest_command=command.data,
            plan_so_far=plan_so_far,
            library=new_library,
            solver_parameters=session.solver_parameters,
            chain_id=session.chain_id,
            llm_user=session.llm_user,
            st_model=session.st_model,
            plan_database=session.plan_database,
            device=session.device,
            seed=session.seed,
        )

    return EvaluateSession(
        current_state=current_state,
        library=session.library,
        plan_so_far=session.plan_so_far,
        solver_parameters=session.solver_parameters,
        latest_command=command.data,
        latest_command_plan_candidate=[plan.plan],
        latest_command_np=np,
        plan_before_command=session.plan_so_far,
        task=session.task,
        chain_id=session.chain_id,
        llm_user=session.llm_user,
        st_model=session.st_model,
        plan_database=session.plan_database,
        device=session.device,
        seed=str(_random.random()),
    )
