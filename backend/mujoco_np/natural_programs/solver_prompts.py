"""Prompting utilities for the NP solver."""
from ant_crafter.predicates import Predicate
import ant_crafter.predicates
# Hacky way to expose the entire predicates library when we do `exec()`
from ant_crafter.predicates import *
from ant_crafter.mujoco.map import AbstractMapState
from mujoco_np.natural_programs.types import NaturalLanguageString
from mujoco_np.natural_programs.types import DecompositionDB
from mujoco_np.natural_programs.types import PredicateGroundingExample
from mujoco_np.openai import query_llm
import ast
import re
import inspect


class ResponseCannotBeParsedException(Exception):
    def __init__(self, exception):
        self.exception = exception


class StepCannotBeDecomposedException(Exception):
    pass


def get_predicate_prompt(
    instruction: NaturalLanguageString,
    abstract_state: AbstractMapState,
    library: list[PredicateGroundingExample],
) -> list[dict[str, str]]:
    # TODO: choose most relevant examples from library. For now,
    # use entire library.
    examples = list(library)

    # Construct prompt
    system_prompt = {
        "role": "system",
        "content": "You are a robotics expert, " +
                   "skilled in translating instructions to code."
    }
    lines = list[str]()

    # Add documentation
    lines.append(
        "Below is a DSL for predicates that can be used to" +
        " constrain robot behavior."
    )
    lines.append("```python")
    predicates_dsl = inspect.getsource(ant_crafter.predicates)
    lines.append(predicates_dsl)
    lines.append("```")

    # Encode examples into the prompt
    for i, example in enumerate(examples):
        lines.append(f"EXAMPLE {i+1:}")
        lines.append(example.pretty_str)
        lines.append("")

    lines.append("")
    lines.append("Provide the Python code for the following test.")
    lines.append(
        "It should be a Python function named `f` that returns a " +
        "single predicate using the API as in the examples."
    )
    lines.append("You shall not use any type annotations.")
    lines.append("You shall not use lambda expressions.")
    lines.append("Only write the requested function, nothing else.")
    lines.append(f"Instruction: {instruction}")
    lines.append(f"Current state: {str(abstract_state)}")

    user_prompt = {
        "role": "user",
        "content": "\n".join(lines)
    }

    prompt = [system_prompt, user_prompt]
    return prompt


def get_function_string(response: str) -> str:
    pattern = r'```\S*\s(.*?)```'
    code_blocks = re.findall(pattern, response, re.DOTALL)
    if len(code_blocks) == 0:
        # Assume the entire string is code
        return response
    code_block = code_blocks[0]
    new_lines = [
        line
        for line in code_block.split("\n")
        if "```" not in line
    ]
    return "\n".join(new_lines)


def get_predicate(
    instruction: NaturalLanguageString,
    abstract_state: AbstractMapState,
    library: list[PredicateGroundingExample],
) -> tuple[Predicate, str]:
    """Ground the instruction into a predicate. Returns the predicate
    and the function that generated it as a string.

    The function takes as input a `MapSpec`.
    """
    # Construct prompt
    prompt = get_predicate_prompt(
        instruction=instruction,
        abstract_state=abstract_state,
        library=library,
    )

    # DEBUG PRINT
    print("INSTRUCTION")
    print(instruction)
    #print("PROMPT")
    #print(prompt[1]["content"])
    # /DEBUG

    # Query language model
    response = query_llm(prompt)

    # DEBUG PRINT
    print("RESPONSE")
    print(response)
    # /DEBUG

    # Parse response
    function_string = get_function_string(response)

    # Import all predicates so they are in scope during function recompilation
    try:
        exec(function_string)
    except Exception as e:
        raise ResponseCannotBeParsedException(e)

    # https://stackoverflow.com/a/73326637
    # Parse the string to an ast
    tree = ast.parse(function_string)

    # Get function name
    assert isinstance(tree.body[0], ast.FunctionDef)
    name = tree.body[0].name  # Needs to be changed depending on the input

    # Compile the ast to executable code
    try:
        code = compile(tree, '<string>', 'exec')
    except Exception as e:
        raise ResponseCannotBeParsedException(e)

    # exec the code in a namespace
    ns = dict(globals())
    try:
        exec(code, ns)
    except Exception as e:
        raise ResponseCannotBeParsedException(e)

    # Get the callable function
    function = ns[name]

    predicate = function(abstract_state)
    return predicate, function_string


def get_step_prompt(
    instruction: NaturalLanguageString,
    abstract_state: AbstractMapState,
    decomposition_db: DecompositionDB,
) -> list[dict[str, str]]:
    """Get a prompt for an LLM for the given instruction, using
    the given library."""
    # TODO: choose most relevant examples from library. For now,
    # use entire library.
    examples = list(decomposition_db)

    # Construct prompt
    system_prompt = {
        "role": "system",
        "content": "You are an strategic planner, " +
                   "skilled at decomposing instructions into parts."
    }

    # Encode examples into the prompt
    lines = list[str]()
    for i, example in enumerate(examples):
        lines.append(f"EXAMPLE {i+1:}")
        lines.append(example.pretty_str)
        lines.append("")

    lines.append("")
    lines.append(
        "Provide the steps for the following instruction. " +
        "The steps should follow the same formatting as the examples. " +
        "If the instruction cannot be further decomposed, " +
        "write an empty list." +
        "Note that you should not write a decomposition that only " +
        "contains the instruction." +
        "You will often have to write empty lists."
    )
    lines.append("")
    lines.append("TEST")
    lines.append(f"Instruction: {instruction}")
    lines.append(f"Current state: {str(abstract_state)}")

    user_prompt = {
        "role": "user",
        "content": "\n".join(lines)
    }

    prompt = [system_prompt, user_prompt]
    return prompt


def get_steps_string(response: str) -> str:
    pattern = r'```(.*?)```'
    blocks = re.findall(pattern, response, re.DOTALL)
    if len(blocks) == 0:
        # Assume the entire string is steps
        return response
    block = blocks[0]
    new_lines = [
        line
        for line in block.split("\n")
        if "```" not in line
    ]
    return "\n".join(new_lines)


def get_step_instructions(
    instruction: NaturalLanguageString,
    decomposition_db: DecompositionDB,
    abstract_state: AbstractMapState,
) -> list[NaturalLanguageString]:
    """Use an LLM to sample a list of steps to decompose the given
    instruction."""
    # Construct prompt
    prompt = get_step_prompt(
        instruction=instruction,
        abstract_state=abstract_state,
        decomposition_db=decomposition_db,
    )

    # DEBUG PRINT
    print("INSTRUCTION")
    print(instruction)
    #print("PROMPT")
    #print(prompt[1]["content"])
    # /DEBUG

    # Query language model
    response = query_llm(prompt)

    # DEBUG PRINT
    print("RESPONSE")
    print(response)
    # /DEBUG

    # Parse response
    steps_string = get_steps_string(response)
    step_lines = [
        step_line.strip()
        for step_line in steps_string.split("\n")
    ]

    # Filter responses. Sometimes the LLM returns garbage decompositions,
    # such as a decomposition that only includes a step that is equal to the
    # instruction.
    steps = [
        NaturalLanguageString(step_line)
        for step_line in step_lines
        if len(step_line) > 0 and step_line != instruction
    ]
    if len(steps) < 2:
        raise StepCannotBeDecomposedException()
    return steps
