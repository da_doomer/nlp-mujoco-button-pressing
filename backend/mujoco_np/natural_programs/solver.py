"""Natural Programming solver for the ant crafter environment."""
from ant_crafter.planning import get_plan
from ant_crafter.planning import PlannerOptions
from ant_crafter.predicate_length import get_max_plan_length
from ant_crafter.predicates import Predicate
from ant_crafter.predicates import RobotRollAxisLessThan
from ant_crafter.predicates import RobotPitchAxisLessThan
from ant_crafter.predicates import RobotYawAxisLessThan
from ant_crafter.predicates import And, Always
from ant_crafter.mujoco.map import MapSpec
from ant_crafter.mujoco.map import AbstractMapState
from ant_crafter.mujoco.simulation import SimulationState
from ant_crafter.mujoco.simulation import get_abstract_map_state
from ant_crafter.mujoco.simulation import get_simulation_instance
from ant_crafter.robustness import get_robustness_value
from mujoco_np.natural_programs.types import NaturalLanguageString
from mujoco_np.natural_programs.types import NaturalProgram
from mujoco_np.natural_programs.types import NaturalProgramSolved
from mujoco_np.natural_programs.types import DecompositionExample
from mujoco_np.natural_programs.types import DecompositionDB
from mujoco_np.natural_programs.types import PredicateGroundingExample
from mujoco_np.natural_programs.solver_prompts import ResponseCannotBeParsedException, StepCannotBeDecomposedException
from mujoco_np.natural_programs.solver_prompts import get_predicate
from mujoco_np.natural_programs.solver_prompts import get_step_instructions
from mujoco_np.solution_database.nearest_neighbor import get_nearest_solution
import traceback
import math
import random
import numpy
from dataclasses import dataclass
from pathlib import Path


@dataclass
class NPSolverOutput:
    plan: NaturalProgramSolved[Predicate, list[float]]
    is_sat: bool


def instruction_to_np(
    instruction: NaturalLanguageString,
    abstract_state: AbstractMapState,
    predicate_grounding_library: list[PredicateGroundingExample],
    llm_attempt_n: int,
) -> NaturalProgram[Predicate]:
    """Ground the instruction into Natural Program."""
    for _ in range(llm_attempt_n):
        try:
            predicate, predicate_generator_str = get_predicate(
                instruction,
                abstract_state,
                predicate_grounding_library,
            )
        except ResponseCannotBeParsedException as e:
            print(f"LLM response cannot be parsed (attempt {_}/{llm_attempt_n}")
            print("Stack trace:")
            print(traceback.format_exc())
            print("Exception:")
            print(e)
            if _ < llm_attempt_n:
                continue
            else:
                raise e
        np = NaturalProgram(
            instruction=instruction,
            predicate=predicate,
            predicate_generator_str=predicate_generator_str,
        )
        return np


def get_steps(
    np: NaturalProgram[Predicate],
    map_spec: MapSpec,
    decomposition_library: DecompositionDB,
    predicate_grounding_library: list[PredicateGroundingExample],
    simulation_state: SimulationState,
    sub_step_s: float,
) -> list[NaturalProgram[Predicate]]:
    """Get recursive steps for the given natural program."""
    # Assemble step database
    decomposition_db = [
        DecompositionExample(
            instruction=step_np.instruction,
            steps=[
                step_np
                for step_np in step_np.steps
            ],
            map_spec=step_np.map_spec,
            simulation_state=simulation_state,
        )
        for step_np in decomposition_library
    ]

    # Get current abstract state
    sim = get_simulation_instance(
        map_spec=map_spec,
        animation_fps=None,
        sub_step_s=sub_step_s,
    )
    sim.state = simulation_state
    abstract_state = get_abstract_map_state(sim)

    # Prompt LLM for decomposition
    step_instructions = get_step_instructions(
        instruction=np.instruction,
        decomposition_db=decomposition_db,
        abstract_state=abstract_state,
    )

    # Turn step instructions into Natural Programs
    step_nps = [
        instruction_to_np(
            instruction=instruction,
            abstract_state=abstract_state,
            predicate_grounding_library=predicate_grounding_library,
            llm_attempt_n=3,
        )
        for instruction in step_instructions
    ]
    return step_nps


def get_flattened_solved_np(
    np: NaturalProgramSolved[Predicate, list[float]]
) -> list[list[float]]:
    """Recursively find all the actions executed by the given solved
    NP."""
    actions = list[list[float]]()
    for step in np.steps:
        if isinstance(step, NaturalProgramSolved):
            step_actions = get_flattened_solved_np(step)
            actions.extend(step_actions)
        else:
            actions.append(step)
    return actions


def get_ant_is_upright_predicate(length: int) -> Predicate:
    roll_axis_ok = RobotRollAxisLessThan(math.pi/3)
    pitch_axis_ok = RobotPitchAxisLessThan(math.pi/3)
    #yaw_axis_ok = RobotYawAxisLessThan(math.pi/3)
    ant_is_upright = And(
        roll_axis_ok,
        pitch_axis_ok,
    )
    predicate = Always(
        min_t=0,
        max_t=length,
        predicate=ant_is_upright,
    )
    return predicate


def solve(
    np: NaturalProgram[Predicate],
    predicate_grounding_library: list[PredicateGroundingExample],
    decomposition_library: DecompositionDB,
    map_spec: MapSpec,
    simulation_state: SimulationState,
    solver_max_iter_n: int,
    sub_step_s: float,
    max_workers: int,
    planner_options: PlannerOptions,
    seed: str,
    st_model: str,  # SentenceTransformers model
    plan_database: Path,
    device: str,
    verbose: bool,
) -> NPSolverOutput:
    """Get a plan that solves the given natural program."""
    _random = random.Random(seed)

    # Try to solve directly
    # We use the solution to the most similar problem in the nearest
    # neighbors database to initialize the search
    plan_len = get_max_plan_length(np.predicate, min_length=10)
    plan_shape: tuple[int, int] = (plan_len, map_spec.robot.actuator_n)
    nearest_solution = get_nearest_solution(
        instruction=np.instruction,
        state=simulation_state,
        predicate=np.predicate,
        map_spec=map_spec,
        plan_shape=plan_shape,
        solution_database_file=plan_database,
        model=st_model,
        device=device,
    )
    if verbose:
        lines = [
            f"[CMA-ES planner] Using cached plan as initial candidate",
            f"                 Solving:     NP(instruction='{np.instruction}', ...)",
            f"                 Cached plan: NP(instruction='{nearest_solution.instruction}', ...)",
        ]
        print("\n".join(lines))
    initial_candidate = [
        list(li)
        for li in nearest_solution.plan
    ]
    plan = get_plan(
        # Solve a slightly different predicate to enforce "good behavior"
        predicate=And(np.predicate, get_ant_is_upright_predicate(plan_len)),
        map_spec=map_spec,
        simulation_state=simulation_state,
        plan_len=plan_len,
        max_iter_n=solver_max_iter_n,
        max_workers=max_workers,
        sub_step_s=sub_step_s,
        options=planner_options,
        seed=str(_random.random()),
        initial_candidate=initial_candidate,
        verbose=verbose,
    )
    if plan.is_sat:
        np_solved = NaturalProgramSolved[Predicate, list[float]](
            natural_program=np,
            steps=plan.plan,
            map_spec=map_spec,
            simulation_state=simulation_state,
        )
        planner_output = NPSolverOutput(
            plan=np_solved,
            is_sat=True,
        )
        return planner_output

    # The planner failed to solve directly. Try to decompose the instruction
    try:
        steps = get_steps(
            np=np,
            map_spec=map_spec,
            decomposition_library=decomposition_library,
            predicate_grounding_library=predicate_grounding_library,
            simulation_state=simulation_state,
            sub_step_s=sub_step_s,
        )
    except StepCannotBeDecomposedException:
        # The instruction could not be decomposed. Simply return
        # the best plan
        np_solved = NPSolverOutput(
            plan=NaturalProgramSolved(
                natural_program=np,
                steps=plan.plan,
                map_spec=map_spec,
                simulation_state=simulation_state,
            ),
            is_sat=False,
        )
        return np_solved

    # Recursively solve each step in the decomposition
    state = simulation_state
    plan = list[NaturalProgramSolved[Predicate, list[float]]]()
    for step in steps:
        # Solve step
        # TODO: what happens if the step is unsuccessful?
        # Several options:
        # - Try another decomposition (how many?)
        # - Propagate failure (i.e., only try one decomposition)
        # - Ignore failure (hope that its a false failure and next steps
        #   fix it)
        # For now, I'm ignoring failure.
        step_actions = solve(
            np=step,
            map_spec=map_spec,
            simulation_state=state,
            solver_max_iter_n=solver_max_iter_n,
            max_workers=max_workers,
            sub_step_s=sub_step_s,
            predicate_grounding_library=predicate_grounding_library,
            decomposition_library=decomposition_library,
            planner_options=planner_options,
            seed=str(_random.random()),
            st_model=st_model,
            plan_database=plan_database,
            device=device,
            verbose=verbose,
        )
        plan.append(step_actions.plan)

        # Step simulation
        sim = get_simulation_instance(
            map_spec=map_spec,
            animation_fps=None,
            sub_step_s=sub_step_s,
        )
        sim.state = state
        for action in get_flattened_solved_np(step_actions.plan):
            sim.step(numpy.array(action))
        state = sim.state

    # Assemble complete solved NP
    solved_np = NaturalProgramSolved[Predicate, list[float]](
        natural_program=np,
        steps=plan,
        map_spec=map_spec,
        simulation_state=simulation_state,
    )

    # Verify the global plan satisfies the top-level predicate
    robustness = get_robustness_value(
        plan=get_flattened_solved_np(solved_np),
        map_spec=map_spec,
        simulation_state=simulation_state,
        predicate=np.predicate,
        t=0,
        sub_step_s=sub_step_s,
    )
    if robustness < 0.0:
        is_sat = False
    else:
        is_sat = True
    planner_output = NPSolverOutput(
        plan=solved_np,
        is_sat=is_sat,
    )
    return planner_output
