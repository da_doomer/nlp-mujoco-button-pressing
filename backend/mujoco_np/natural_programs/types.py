"""Type definitions for natural programs."""
from dataclasses import dataclass
from typing import NewType
from typing import Generic
from typing import TypeVar
from ant_crafter.mujoco.map import MapSpec
from ant_crafter.mujoco.simulation import SimulationState


NaturalLanguageString = NewType("NaturalLanguageString", str)
Predicate = TypeVar("Predicate")
Action = TypeVar("Action")


@dataclass
class NaturalProgram(Generic[Predicate]):
    instruction: NaturalLanguageString
    predicate: Predicate
    predicate_generator_str: str


@dataclass
class NaturalProgramSolved(Generic[Predicate, Action]):
    natural_program: NaturalProgram[Predicate]
    steps: (
        list[Action]
        | list["NaturalProgramSolved[Predicate, Action]"]
        | list["NaturalProgramSolved[Predicate, Action]" | Action]
    )
    map_spec: MapSpec
    simulation_state: SimulationState


@dataclass
class PredicateGroundingExample:
    instruction: str
    predicate_generator_str: str
    map_spec: MapSpec
    simulation_state: SimulationState

    @property
    def pretty_str(self) -> str:
        lines = list[str]()
        lines.append(f"Instruction: {self.instruction}")
        lines.append(f"map_spec: {str(self.map_spec)}")
        lines.append("Python code:")
        lines.append("```python")
        lines.append(self.predicate_generator_str)
        lines.append("```")
        return "\n".join(lines)


@dataclass
class DecompositionExample:
    instruction: NaturalLanguageString
    steps: list[NaturalLanguageString]
    map_spec: MapSpec
    simulation_state: SimulationState

    @property
    def pretty_str(self) -> str:
        lines = list[str]()
        lines.append(f"Instruction: {self.instruction}")
        lines.append("```")
        lines.append("\n".join(self.steps))
        lines.append("```")
        return "\n".join(lines)


GroundingDB = list[NaturalProgram[Predicate]]
PredicateGroundingDB = list[PredicateGroundingExample]
DecompositionDB = list[DecompositionExample]
