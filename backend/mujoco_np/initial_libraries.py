"""Hard-coded initial library generation."""
from ant_crafter.mujoco.map import AbstractMapState
from ant_crafter.predicates import ItemAtLocation
from ant_crafter.predicates import RobotContactWithItem
from ant_crafter.predicates import RobotAtLocation
from ant_crafter.predicates import ItemInContactWithItem
from ant_crafter.predicates import Eventually
from ant_crafter.predicates import And
from ant_crafter.predicates import Or
from ant_crafter.predicates import Predicate
from ant_crafter.map_parser import get_map_spec
from ant_crafter.mujoco.simulation import get_simulation_instance
from mujoco_np.natural_programs.types import DecompositionExample
from mujoco_np.natural_programs.types import NaturalLanguageString
from mujoco_np.natural_programs.types import PredicateGroundingExample
from mujoco_np.natural_programs.types import DecompositionDB
from pathlib import Path
import inspect


example_map_file = Path(__file__).parent.parent/"lib"/"ant-crafter"/"resources"/"example_map.json"
assert example_map_file.exists()


# "Go to first button"
def f_example1(abstract_state: AbstractMapState):
    button_position = abstract_state.button_positions[0]
    button_x, button_y = button_position
    predicate = Eventually(
        min_t=0,
        max_t=10,
        predicate=RobotAtLocation(
            target_x=button_x,
            target_y=button_y,
        ))
    return predicate


# "Go to the second button then to the first button"
def f_example2(abstract_state: AbstractMapState):
    button1_position = abstract_state.button_positions[0]
    button1_x, button1_y = button1_position
    button2_position = abstract_state.button_positions[1]
    button2_x, button2_y = button2_position
    predicate1 = Eventually(
        min_t=0,
        max_t=10,
        predicate=RobotAtLocation(
            target_x=button1_x,
            target_y=button1_y,
        )
    )
    predicate2 = Eventually(
        min_t=10,
        max_t=20,
        predicate=RobotAtLocation(
            target_x=button2_x,
            target_y=button2_y,
        )
    )
    predicate3 = And(
        predicate1,
        predicate2,
    )
    return predicate3


# "Move item 'cubelarge' to (0.0, 0.6)"
def f_example3(abstract_state: AbstractMapState):
    contact_item = Eventually(
        min_t=0,
        max_t=10,
        predicate=RobotContactWithItem(item_name='cubelarge'),
    )
    place_item = Eventually(
        min_t=10,
        max_t=20,
        predicate=ItemAtLocation(
            item_name='cubelarge',
            target_x=0.0,
            target_y=0.6,
        ),
    )
    move_item = And(
        contact_item,
        place_item,
    )
    return move_item


# Make item 'cubelarge' and item 'teapot' collide by pushing either of them
def f_example4(abstract_state: AbstractMapState):
    # Reach either item (or of contact). This makes the optimization problem
    # easier
    contact_cubelarge = Eventually(
        min_t=0,
        max_t=10,
        predicate=RobotContactWithItem(item_name='cubelarge'),
    )
    contact_teapot = Eventually(
        min_t=0,
        max_t=10,
        predicate=RobotContactWithItem(item_name='teapot'),
    )
    contact = Or(
        contact_cubelarge,
        contact_teapot,
    )

    # Make the items collide
    item_item_contact = Eventually(
        min_t=10,
        max_t=20,
        predicate=ItemInContactWithItem(
            item1_name='cubelarge',
            item2_name='teapot',
        )
    )

    # Both should be true
    predicate = And(
        contact,
        item_item_contact,
    )
    return predicate


# Go to the position of 'cubelarge'
def f_example5(abstract_state: AbstractMapState):
    # Find the 'cubelarge' item
    item = next(item for item in abstract_state.items if item.item_name == 'cubelarge')

    # Extract the target position
    target_x, target_y = item.position.x, item.position.y
    predicate = Eventually(
        min_t=0,
        max_t=10,
        predicate=RobotAtLocation(
            target_x=target_x,
            target_y=target_y,
        )
    )
    return predicate


def get_initial_predicate_grounding_library(
) -> list[PredicateGroundingExample]:
    """Get a list with a couple hand-written instruction examples."""
    map_spec = get_map_spec(example_map_file)
    simulation_state = get_simulation_instance(
        map_spec=map_spec,
        animation_fps=None,
        sub_step_s=1.0/60.0,
    ).state
    examples = [
        PredicateGroundingExample(
            instruction="Go to the first button",
            map_spec=map_spec,
            predicate_generator_str=inspect.getsource(f_example1),
            simulation_state=simulation_state,
        ),
        PredicateGroundingExample(
            instruction="Go to the second button then to the first button",
            map_spec=map_spec,
            predicate_generator_str=inspect.getsource(f_example2),
            simulation_state=simulation_state,
        ),
        PredicateGroundingExample(
            instruction="Move item 'cubelarge' to (0.0, 0.6)",
            map_spec=map_spec,
            predicate_generator_str=inspect.getsource(f_example3),
            simulation_state=simulation_state,
        ),
        PredicateGroundingExample(
            instruction="Make item 'cubelarge' and item 'teapot' collide by pushing either of them",
            map_spec=map_spec,
            predicate_generator_str=inspect.getsource(f_example4),
            simulation_state=simulation_state,
        ),
        PredicateGroundingExample(
            instruction="Go to the position of item 'cubelarge'",
            map_spec=map_spec,
            predicate_generator_str=inspect.getsource(f_example5),
            simulation_state=simulation_state,
        ),
    ]
    return examples


def get_initial_steps_library() -> DecompositionDB:
    """Get a hard-coded small library of instruction decompositions."""
    library: DecompositionDB = list()

    # Load simple example map
    map_spec = get_map_spec(example_map_file)
    simulation_state = get_simulation_instance(
        map_spec=map_spec,
        animation_fps=None,
        sub_step_s=1.0/60.0,
    ).state

    # Example 1
    example = DecompositionExample(
        instruction=NaturalLanguageString(
            "Go to first button then go to second button"
        ),
        steps=[
            NaturalLanguageString("Go to first button"),
            NaturalLanguageString("Go to second button"),
        ],
        map_spec=map_spec,
        simulation_state=simulation_state,
    )
    library.append(example)

    # Example 2
    example = DecompositionExample(
        instruction=NaturalLanguageString(
            "Go to third button then go to first button"
        ),
        steps=[
            NaturalLanguageString("Go to third button"),
            NaturalLanguageString("Go to first button"),
        ],
        map_spec=map_spec,
        simulation_state=simulation_state,
    )
    library.append(example)

    # Example 3
    example = DecompositionExample(
        instruction=NaturalLanguageString("Go to first button"),
        steps=[],
        map_spec=map_spec,
        simulation_state=simulation_state,
    )
    library.append(example)

    return library
