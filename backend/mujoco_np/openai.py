"""OpenAI LLM-querying facade."""
from openai import OpenAI
import dbm
import json
from pathlib import Path


database_file = Path(__file__).parent/"database"


def _query_openai(prompt: list[dict[str, str]]) -> str:
    client = OpenAI()
    completion = client.chat.completions.create(
      model="gpt-3.5-turbo",
      messages=prompt,
    )
    response = completion.choices[0].message.content
    assert isinstance(response, str), "API did not return valid response!"
    return response


def query_llm(prompt: list[dict[str, str]]) -> str:
    key = bytes(json.dumps(prompt), "utf-8")
    with dbm.open(str(database_file.resolve()), "c") as db:
        # If prompt was serialized, return cached response
        if key in db.keys():
            return db[key].decode("utf-8")
        response = _query_openai(prompt)
        db[key] = bytes(response, "utf-8")
    return response
