"""Test script to solve predicates that correspond to compound instructions
like "Go to first button then go to second button"."""
import ant_crafter.mujoco.simulation as simulation
from ant_crafter.map_parser import get_map_spec
from ant_crafter.planning import get_plan
from ant_crafter.planning import PlannerOptions
from ant_crafter.predicate_length import get_max_plan_length
from pathlib import Path
from mujoco_np.initial_libraries import get_initial_predicate_grounding_library
from mujoco_np.natural_programs.solver_prompts import get_predicate
from mujoco_np.natural_programs.types import NaturalLanguageString
import argparse
import numpy as np


def main(
    map_file: Path,
    output_dir: Path,
    max_workers: int,
    planner_options: PlannerOptions,
):
    # Get user instruction
    #instruction = input("Instruction: ")
    instruction = "Go to first button then go to second button"

    # Construct simulation spec
    map_spec = get_map_spec(map_file)

    # Get starter library
    library = get_initial_predicate_grounding_library()

    # Turn instruction into predicate
    predicate, _ = get_predicate(
        instruction=NaturalLanguageString(instruction),
        map_spec=map_spec,
        library=library,
    )

    # Call solver
    sub_step_s = 1.0/10
    plan_len = get_max_plan_length(predicate, min_length=10)
    initial_state = simulation.get_simulation_instance(
        map_spec=map_spec,
        animation_fps=None,
        sub_step_s=sub_step_s,
    ).state
    plan = get_plan(
        predicate=predicate,
        map_spec=map_spec,
        simulation_state=initial_state,
        max_iter_n=1000,
        plan_len=plan_len,
        max_workers=max_workers,
        options=planner_options,
        verbose=True,
        sub_step_s=sub_step_s,
        seed="asd",
    )

    # Plot result
    sim = simulation.get_simulation_instance(
        map_spec=map_spec,
        animation_fps=60.0,
        sub_step_s=sub_step_s,
    )
    for action in plan.plan:
        sim.step(np.array(action))
    for _ in range(10):
        # Extra "zero action" steps for animation purposes
        sim.step(np.zeros((map_spec.robot.actuator_n)))

    video_output = output_dir/"video.mp4"
    sim.save_video(video_output)
    print(f"Wrote {video_output}")


if __name__ == "__main__":
    parser = argparse.ArgumentParser(
        description='Execute compound instruction.'
    )
    parser.add_argument(
        '--map',
        type=Path,
        required=True,
        help='Input map file.',
    )
    parser.add_argument(
        '--max_workers',
        type=int,
        required=True,
        help='Number of workers for the optimization.',
    )
    parser.add_argument(
        '--planner_popsize',
        type=int,
        required=True,
        help='Population size for the planner.',
    )
    parser.add_argument(
        '--output_dir',
        type=Path,
        required=True,
        help='Output directory.',
    )
    args = parser.parse_args()
    args.output_dir.mkdir(exist_ok=False)

    # Parse planner options
    planner_options = PlannerOptions(
        popsize=args.planner_popsize,
    )

    main(
        map_file=args.map,
        output_dir=args.output_dir,
        max_workers=args.max_workers,
        planner_options=planner_options,
    )
