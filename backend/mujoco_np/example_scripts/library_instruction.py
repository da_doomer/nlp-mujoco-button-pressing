"""Test script to ground instructions like "Go to first button then go to
second button" into natural programs."""
import ant_crafter.mujoco.simulation as simulation
from ant_crafter.map_parser import get_map_spec
from ant_crafter.planning import PlannerOptions
from pathlib import Path
from mujoco_np.natural_programs.solver import instruction_to_np
from mujoco_np.natural_programs.solver import solve
from mujoco_np.natural_programs.solver import get_flattened_solved_np
from mujoco_np.initial_libraries import get_initial_predicate_grounding_library
from mujoco_np.initial_libraries import get_initial_steps_library
from mujoco_np.natural_programs.types import NaturalLanguageString
import argparse
import numpy as np


def main(
    map_file: Path,
    output_dir: Path,
    max_workers: int,
    planner_options: PlannerOptions,
):
    # Get user instruction
    #instruction = input("Instruction: ")
    #instruction = "Go to first button then go to second button in 20 timesteps"
    #instruction = "Go to the initial position of the first item in 20 timesteps"
    #instruction = "Move item 'cubelarge' to (0.38, 0.6)"
    instruction = "Make item 'cubelarge' and item 'teapot' collide by pushing either of them"

    # Construct simulation spec
    map_spec = get_map_spec(map_file)

    # Get starter instruction library
    predicate_grounding_library = get_initial_predicate_grounding_library()

    # Get Natural Program
    natural_program = instruction_to_np(
        instruction=NaturalLanguageString(instruction),
        map_spec=map_spec,
        predicate_grounding_library=predicate_grounding_library,
    )

    # Get starter solving database
    decomposition_library = get_initial_steps_library()

    # Call solver
    sub_step_s = 1.0/10
    initial_state = simulation.get_simulation_instance(
        map_spec=map_spec,
        animation_fps=None,
        sub_step_s=1.0/10,
    ).state
    plan = solve(
        np=natural_program,
        decomposition_library=decomposition_library,
        predicate_grounding_library=predicate_grounding_library,
        map_spec=map_spec,
        simulation_state=initial_state,
        solver_max_iter_n=100,
        sub_step_s=sub_step_s,
        max_workers=max_workers,
        planner_options=planner_options,
        seed="asd",
        verbose=True,
    )

    # Plot result
    sim = simulation.get_simulation_instance(
        map_spec=map_spec,
        animation_fps=60.0,
        sub_step_s=sub_step_s,
    )
    for action in get_flattened_solved_np(plan.plan):
        sim.step(np.array(action))
    for _ in range(10):
        # Extra "zero action" steps for animation purposes
        sim.step(np.zeros((map_spec.robot.actuator_n)))

    video_output = output_dir/"video.mp4"
    sim.save_video(video_output)
    print(f"Wrote {video_output}")


if __name__ == "__main__":
    parser = argparse.ArgumentParser(
        description='Execute NP with initial library.'
    )
    parser.add_argument(
        '--map',
        type=Path,
        required=True,
        help='Input map file.',
    )
    parser.add_argument(
        '--planner_popsize',
        type=int,
        required=True,
        help='Population size for the planner.',
    )
    parser.add_argument(
        '--max_workers',
        type=int,
        required=True,
        help='Number of workers for the planner.',
    )
    parser.add_argument(
        '--output_dir',
        type=Path,
        required=True,
        help='Output directory.',
    )
    args = parser.parse_args()
    args.output_dir.mkdir(exist_ok=False)

    # Parse planner options
    planner_options = PlannerOptions(
        popsize=args.planner_popsize,
    )

    main(
        map_file=args.map,
        output_dir=args.output_dir,
        planner_options=planner_options,
        max_workers=args.max_workers,
    )
