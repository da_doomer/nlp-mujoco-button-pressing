"""Firebase facade for messages."""
from dataclasses import dataclass
import firebase_admin
from firebase_admin import credentials
from firebase_admin import firestore
from firebase_admin import storage
from google.cloud.firestore_v1.base_query import FieldFilter
from pathlib import Path
from mujoco_np.messages.types import Response
from mujoco_np.messages.types import Prompt
from mujoco_np.messages.types import Image
from mujoco_np.messages.types import Video
from mujoco_np.interaction_loop.session_types import Session
import tempfile
import time
import hashlib
import jsonpickle


NO_PREVIOUS_SESSION = "NO_PREVIOUS_SESSION"


cred = credentials.Certificate(
    (Path(__file__).parent/"credentials.json").resolve()
)
app = firebase_admin.initialize_app(cred, {
    'storageBucket': 'np-mujoco.appspot.com'
})
db = firestore.client()
bucket = storage.bucket()

SPIN_LOCK_SLEEP_S = 1.0
CHAIN_IDS_COLLECTION = 'chain_ids'
PROMPTS_COLLECTION = 'prompts'
RESPONSES_COLLECTION = 'responses'
SESSIONS_COLLECTION = 'sessions'
SESSION_OUTCOMES_COLLECTION = 'session_outcomes'

VIDEO_SUFFIXES = [
    ".mp4",
    ".webm",
    ]

IMAGE_SUFFIXES = [
    ".png",
    ".jpg",
    ]


@dataclass
class StoredResponse:
    response: Response
    response_id: str


@dataclass
class StoredInteraction:
    prompt_id: str
    response_id: str
    response: Response
    prompt: Prompt


class PromptResponseTimeoutError(Exception):
    """Raised when a prompt response is not received within the time
    budget."""
    pass


def store_bytes(
    data: bytes,
    storage_blob_id: str,
) -> None:
    """Returns `None`."""
    with tempfile.TemporaryDirectory() as tmpdirname:
        file_path = Path(tmpdirname)/"file"
        with open(file_path, "wb") as fp:
            fp.write(data)
        blob = bucket.blob(storage_blob_id)
        blob.upload_from_filename(str(file_path.resolve()))


def get_bytes(
    storage_blob_id: str,
) -> bytes:
    blob = bucket.blob(storage_blob_id)
    data = blob.download_as_bytes()
    return data


def wait_for_db_response(
    prompt_id: str,
    timeout_s: float,
) -> StoredResponse:
    # Assemble query for responses to the given prompt
    responses_ref = db.collection(RESPONSES_COLLECTION)
    query = responses_ref.where(filter=FieldFilter(
        "prompt_id", "==", prompt_id
    ))

    # Wait for a response. Spin lock. A bit wasteful but Firebase's API
    # is not super great for this kind of thing (because I'm abusing it
    # as a message queue when it's a database).
    wait_start_t = time.time()
    responses = list(query.stream())
    while len(responses) == 0:
        time.sleep(SPIN_LOCK_SLEEP_S)
        if time.time() - wait_start_t > timeout_s:
            raise PromptResponseTimeoutError()
        responses = list(query.stream())

    # Parse response
    raw_response = responses[0].to_dict()
    response_id = responses[0].id
    assert raw_response is not None, "Response in database cannot be parsed!"
    assert isinstance(response_id, str), f"Response ID '{response_id}' is not a string!"
    response = Response(
        data=raw_response["data"],
    )
    stored_response = StoredResponse(
        response_id=response_id,
        response=response,
    )
    return stored_response


def get_hash(b: bytes) -> str:
    m = hashlib.sha256()
    m.update(b)
    return m.hexdigest()


def store_response(
    response: Response,
    prompt_id: str,
) -> str:
    """Returns the Response's storage key."""
    # Store response
    responses_ref = db.collection(RESPONSES_COLLECTION)
    _, new_response_ref = responses_ref.add({
        'data': response.data,
        'prompt_id': prompt_id,
    })

    # Return ID
    response_id = new_response_ref.id
    return response_id


def store_prompt(
    prompt: Prompt,
    chain_id: str,
) -> str:
    """Returns the Prompt's storage key."""
    # Assemble prompt
    data = list[dict[str, str]]()

    for datum in prompt.data:
        if isinstance(datum, str):
            data.append({
                "message": datum,
                "type": "string",
                })
        elif isinstance(datum, Video):
            storage_blob_id = get_hash(datum.data)
            store_bytes(
                data=datum.data,
                storage_blob_id=storage_blob_id,
            )
            data.append({
                "message": storage_blob_id,
                "type": "video",
                })
        elif isinstance(datum, Image):
            storage_blob_id = get_hash(datum.data)
            store_bytes(
                data=datum.data,
                storage_blob_id=storage_blob_id,
            )
            data.append({
                "message": storage_blob_id,
                "type": "image",
                })
        else:
            raise ValueError(
                f"Prompt datum of type '{type(datum)}' not recognized!"
            )

    # Push prompt
    prompts_ref = db.collection(PROMPTS_COLLECTION)
    _, new_prompt_ref = prompts_ref.add({
        'data': data,
        'chain_id': chain_id,
        'has_response': False,
        'timestamp': firestore.SERVER_TIMESTAMP,
    })
    prompt_id = new_prompt_ref.id
    assert isinstance(prompt_id, str), "Firebase did not assign key to prompt!"
    return prompt_id


def store_chain_id(chain_id: str) -> None:
    sessions_ref = db.collection(CHAIN_IDS_COLLECTION)
    sessions_ref.add({
        'chain_id': chain_id,
    })


def get_chain_ids() -> list[str]:
    chain_ids_ref = db.collection(CHAIN_IDS_COLLECTION)
    docs = chain_ids_ref.get()
    chain_ids = list[str]()
    for doc in docs:
        chain_id: str = doc.to_dict()['chain_id']
        chain_ids.append(chain_id)
    return chain_ids


def store_session(
    session: Session,
) -> str:
    """Returns the session ID."""
    # Get session bytes
    session_jsonpickle: str = jsonpickle.encode(
        session,
        make_refs=False,
        warn=True,
    )
    session_bytes = session_jsonpickle.encode('utf-8')

    # Get initial state bytes
    with tempfile.TemporaryDirectory() as tempdirname:
        initial_state_image_path = Path(tempdirname)/"current_state.png"
        session.save_current_state_image(initial_state_image_path)
        datum = Image.from_path(initial_state_image_path)
        initial_state_image_bytes = datum.data

    # Store session data
    session_blob_id = get_hash(session_bytes)
    initial_image_storage_blob_id = get_hash(initial_state_image_bytes)
    store_bytes(initial_state_image_bytes, initial_image_storage_blob_id)
    store_bytes(session_bytes, session_blob_id)
    sessions_ref = db.collection(SESSIONS_COLLECTION)
    _, new_session_ref = sessions_ref.add({
        'session_blob_id': session_blob_id,
        'initial_image_blob_id': initial_image_storage_blob_id,
        'timestamp': firestore.SERVER_TIMESTAMP,
        'chain_id': session.chain_id,
    })

    # Return ID
    session_id = new_session_ref.id
    return session_id


def get_session(
    session_id: str,
) -> Session:
    # Assemble query for responses to the given prompt
    sessions_ref = db.collection(SESSIONS_COLLECTION)
    doc_ref = sessions_ref.document(session_id)
    doc = doc_ref.get()
    if not doc.exists:
        raise KeyError(f"Session ID '{session_id}' does not exist!")
    json_blob_id = doc.to_dict()['session_blob_id']
    json_bytes = get_bytes(json_blob_id)
    json_str = json_bytes.decode('utf-8')
    session: Session = jsonpickle.decode(json_str)
    return session


def get_latest_session_id(
    chain_id: str,
) -> str:
    # Assemble query for responses to the given prompt
    sessions_ref = db.collection(SESSIONS_COLLECTION)
    query = sessions_ref.where(
        filter=FieldFilter("chain_id", "==", chain_id),
    ).order_by("timestamp").limit_to_last(1)
    results = query.get()
    if len(results) == 0:
        raise KeyError(f"Chain ID '{chain_id}' has no associated sessions!")
    session_id = results[0].id
    return session_id


def store_session_outcome(
    session_id: str,
    previous_session_id: str,
    interaction_prompt_id: str,
    interaction_response_id: str,
    next_session: Session,
) -> str:
    """Returns the session ID."""
    # Get bytes of the full video
    with tempfile.TemporaryDirectory() as tempdirname:
        video_so_far_path = Path(tempdirname)/"current_state.mp4"
        next_session.save_full_video(video_so_far_path)
        datum = Video.from_path(video_so_far_path)
        video_bytes = datum.data

    # Store session outcome data
    video_blob_id = get_hash(video_bytes)
    store_bytes(video_bytes, video_blob_id)
    session_outcomes_ref = db.collection(SESSION_OUTCOMES_COLLECTION)
    _, new_session_outcome_ref = session_outcomes_ref.add({
        'session_id': session_id,
        'previous_session_id': previous_session_id,
        'interaction_prompt_id': interaction_prompt_id,
        'interaction_response_id': interaction_response_id,
        'full_video_blob_id': video_blob_id,
        'timestamp': firestore.SERVER_TIMESTAMP,
    })

    # Return ID
    session_outcome_id: str = new_session_outcome_ref.id
    error_message = f"Firebase-given session outcome ID '{session_outcome_id}' is not a string!"
    assert isinstance(session_outcome_id, str), error_message
    return session_outcome_id
