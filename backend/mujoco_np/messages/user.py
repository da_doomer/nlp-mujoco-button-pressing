"""Facade for prompting humans and LLMs for responses to prompts."""
from ant_crafter.mujoco.simulation import get_abstract_map_state
from ant_crafter.mujoco.simulation import get_simulation_instance
from mujoco_np.interaction_loop.session_types import NPLibrary, Session
from mujoco_np.interaction_loop.session_types import EvaluateSession
from mujoco_np.interaction_loop.session_types import FixSession
from mujoco_np.interaction_loop.session_types import ExtendSession
from mujoco_np.interaction_loop.instructions import get_instructions
from mujoco_np.messages.types import Prompt
from mujoco_np.messages.types import Response
from mujoco_np.messages.types import Image
from mujoco_np.messages.types import Video
from mujoco_np.messages.firebase import StoredInteraction
from mujoco_np.messages.firebase import StoredResponse
from mujoco_np.messages.firebase import store_response
from mujoco_np.messages.firebase import store_prompt
from mujoco_np.messages.firebase import wait_for_db_response
from mujoco_np.openai import query_llm
from mujoco_np.natural_programs.solver import get_flattened_solved_np
import numpy as np
from pathlib import Path
import tempfile


def get_instruction_examples(library: NPLibrary) -> list[str]:
    """Get examples of instructions in the library."""
    instructions = set[str]()

    # Add examples from the predicate grounding library
    for predicate_grounding in library.predicate_grounding_library:
        instruction = predicate_grounding.instruction
        instructions.add(instruction)

    # Add examples from the decomposition examples
    for decomposition in library.decomposition_library:
        instruction = decomposition.instruction
        instructions.add(instruction)
        for step in decomposition.steps:
            instructions.add(step)

    # TODO: use semantic similarity to rank the examples
    return list(instructions)


def get_initial_prompt(
    session: Session,
    llm_user: bool,
) -> Prompt:
    # LLM prompts cannot have images
    if not llm_user:
        with tempfile.TemporaryDirectory() as tempdirname:
            current_state_image_path = Path(tempdirname)/"current_state.png"
            session.save_current_state_image(current_state_image_path)
            current_state = Image.from_path(current_state_image_path)
    else:
        sim = get_simulation_instance(
            map_spec=session.task.map_spec,
            animation_fps=None,
            sub_step_s=session.solver_parameters.sub_step_s,
        )
        abstract_state = get_abstract_map_state(sim)
        current_state = str(abstract_state)

    instruction_examples = get_instruction_examples(session.library)
    prompt = Prompt([
        *get_instructions(session.task),
        "This is what the world looks like right now:",
        current_state,
        "Here are some instruction examples:",
        *instruction_examples,
        "What should the robot do first?",
        "You MUST be brief and provide a single, easy to follow instruction.",
        "Hint: usually to craft an item the robot would make contact with an item AND make an item-item-contact between that item and the other item.",
        "Hint: don't focus on coordinates.",
        "Hint: focus on high-level commands (e.g., make item-item-contact between A and B).",
    ])
    return prompt


def get_evaluate_prompt(
    session: EvaluateSession,
    llm_user: bool,
) -> Prompt:
    # LLM prompt cannot include videos
    if not llm_user:
        with tempfile.TemporaryDirectory() as tempdirname:
            latest_command_video_path = Path(tempdirname)/"latest_command.mp4"
            session.save_latest_command_plan_video(latest_command_video_path)
            plan_outcome = Video.from_path(latest_command_video_path)
    else:
        # HACK: TODO: remove
        return Prompt(["Reply YES"])
        sim = get_simulation_instance(
            map_spec=session.task.map_spec,
            animation_fps=None,
            sub_step_s=session.solver_parameters.sub_step_s,
        )
        # Fast-forward plan so far
        for step in session.plan_so_far:
            step_actions = get_flattened_solved_np(step)
            for action in step_actions:
                sim.step(np.array(action))
        # Step latest command plan candidate
        for step in session.latest_command_plan_candidate:
            step_actions = get_flattened_solved_np(step)
            for action in step_actions:
                sim.step(np.array(action))
        abstract_state = get_abstract_map_state(sim)
        plan_outcome = str(abstract_state)

    prompt = Prompt([
        *get_instructions(session.task),
        f"Robot was asked to '{session.latest_command}'.",
        f"This is the outcome of attempting '{session.latest_command}':",
        plan_outcome,
        "Did it succeed? (You MUST respond Yes/No)"
    ])
    return prompt


def get_fix_prompt(
    session: FixSession,
    llm_user: bool,
) -> Prompt:
    # LLM prompt cannot have videos
    if not llm_user:
        with tempfile.TemporaryDirectory() as tempdirname:
            failed_command_plan_candidate_video_path = \
                Path(tempdirname)/"failed_command_plan_candidate.mp4"
            session.save_failed_command_plan_candidate_video(
                failed_command_plan_candidate_video_path,
            )
            failed_plan_outcome = Video.from_path(
                failed_command_plan_candidate_video_path
            )
    else:
        sim = get_simulation_instance(
            map_spec=session.task.map_spec,
            animation_fps=None,
            sub_step_s=session.solver_parameters.sub_step_s,
        )
        # Fast-forward plan so far
        for step in session.plan_so_far:
            step_actions = get_flattened_solved_np(step)
            for action in step_actions:
                sim.step(np.array(action))
        # Step latest command plan candidate
        for step in session.failed_command_plan_candidate:
            step_actions = get_flattened_solved_np(step)
            for action in step_actions:
                sim.step(np.array(action))
        failed_plan_outcome = str(get_abstract_map_state(sim))

    prompt = Prompt([
        *get_instructions(session.task),
        f"Robot was asked to '{session.failed_command}' but failed. ",
        "This is the outcome of the failed plan:",
        failed_plan_outcome,
        "What should the robot do instead?",
        "You MUST be brief and provide a single, easy to follow instruction.",
        "Hint: usually to craft an item the robot would make contact with an item AND make an item-item-contact between that item and the other item.",
        "Hint: don't focus on coordinates.",
        "Hint: focus on high-level commands (e.g., make item-item-contact between A and B).",
    ])
    return prompt


def get_extend_prompt(
    session: ExtendSession,
    llm_user: bool,
) -> Prompt:
    # LLM prompt cannot have videos
    if not llm_user:
        with tempfile.TemporaryDirectory() as tempdirname:
            plan_so_far_video_path = Path(tempdirname)/"plan_so_far.mp4"
            session.save_plan_so_far_video(
                plan_so_far_video_path,
            )
            abstract_state = Video.from_path(plan_so_far_video_path)
    else:
        sim = get_simulation_instance(
            map_spec=session.task.map_spec,
            animation_fps=None,
            sub_step_s=session.solver_parameters.sub_step_s,
        )
        # Fast-forward plan so far
        for step in session.plan_so_far:
            step_actions = get_flattened_solved_np(step)
            for action in step_actions:
                sim.step(np.array(action))
        abstract_state = str(get_abstract_map_state(sim))
    prompt = Prompt([
        *get_instructions(session.task),
        "This is what the world looks like right now.",
        abstract_state,
        "What should the robot do next?",
        "You MUST be brief and provide a single, easy to follow instruction.",
        "Hint: usually to craft an item the robot would make contact with an item AND make an item-item-contact between that item and the other item.",
        "Hint: don't focus on coordinates.",
        "Hint: focus on high-level commands (e.g., make item-item-contact between A and B).",
    ])
    return prompt


def get_llm_prompt(prompt: Prompt) -> list[dict[str, str]]:
    """Transform the given prompt into an OpenAI-compatible prompt."""
    llm_prompt = list[dict[str, str]]()

    # Add system prompt
    llm_prompt.append({
        "role": "system",
        "content": "You are an strategic planner, " +
        "skilled at identifying the most effective tactic available."
    })

    # Add actual prompt
    lines = list[str]()
    for datum in prompt.data:
        if not isinstance(datum, str):
            raise ValueError(
                "Multi-modal prompts not supported!"
            )
        lines.append(datum)
    llm_prompt.append({
        "role": "user",
        "content": "\n".join(lines)
    })

    return llm_prompt


def get_llm_response(
    prompt: Prompt,
    chain_id: str,
) -> StoredResponse:
    # Store prompt
    prompt_id = store_prompt(
        prompt=prompt,
        chain_id=chain_id,
    )

    # Turn prompt into LLM-readable string
    llm_prompt = get_llm_prompt(prompt)

    # Query LLM
    llm_response = query_llm(prompt=llm_prompt)
    response = Response(
        data=llm_response,
    )

    # Store response
    response_id = store_response(
        response,
        prompt_id=prompt_id,
    )
    stored_response = StoredResponse(
        response=response,
        response_id=response_id,
    )
    return stored_response


def ask_user(
    prompt: Prompt,
    chain_id: str,
    timeout_s: float,
    llm_user: bool,
) -> StoredInteraction:
    prompt_id = store_prompt(prompt, chain_id)

    # Wait for response
    print(f"Waiting for response to prompt with ID: '{prompt_id}'")
    if llm_user:
        stored_response = get_llm_response(
            prompt=prompt,
            chain_id=chain_id,
        )
    else:
        stored_response = wait_for_db_response(
            prompt_id=prompt_id,
            timeout_s=timeout_s,
        )
    print(
        f"Received response to prompt with ID '{prompt_id}': " +
        f"{stored_response.response_id}"
    )

    stored_interaction = StoredInteraction(
        prompt_id=prompt_id,
        response_id=stored_response.response_id,
        prompt=prompt,
        response=stored_response.response,
    )
    return stored_interaction
