"""Types for messages between backend and frontend."""
from pathlib import Path
from dataclasses import dataclass


@dataclass
class Image:
    filename: str  # Relative path (e.g., 'image.png')
    data: bytes

    @staticmethod
    def from_path(path: Path) -> "Image":
        filename = path.name
        with open(path, "rb") as fp:
            data = fp.read()
        return Image(
            filename=filename,
            data=data,
        )


@dataclass
class Video:
    filename: str  # Relative path (e.g., 'movie.mp4')
    data: bytes

    @staticmethod
    def from_path(path: Path) -> "Video":
        filename = path.name
        with open(path, "rb") as fp:
            data = fp.read()
        return Video(
            filename=filename,
            data=data,
        )


@dataclass
class Prompt:
    data: list[str | Image | Video]


@dataclass
class Response:
    data: str
