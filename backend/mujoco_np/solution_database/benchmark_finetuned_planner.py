"""Benchmark the fine-tuned LLM-biased numerical planner."""
from ant_crafter.mujoco.map import MapSpec
from ant_crafter.predicates import Eventually, Predicate
from ant_crafter.predicates import RobotContactWithItem
from ant_crafter.predicates import ItemInContactWithItem
from ant_crafter.predicates import And
from ant_crafter.task_sampling import Task
from ant_crafter.predicate_length import get_max_plan_length
from ant_crafter.planning import PlannerOptions, get_plan
from ant_crafter.mujoco.simulation import SimulationState
from ant_crafter.mujoco.simulation import get_simulation_instance
from ant_crafter.task_sampling import Task
from mujoco_np.solution_database.finetune import get_model_plan
from mujoco_np.solution_database.nearest_neighbor import get_nearest_solution
from mujoco_np.solution_database.solutions import get_solved_planning_problem
from mujoco_np.solution_database.solutions import SolvedPlanningProblem
from mujoco_np.solution_database.plot_benchmark import plot_results
from mujoco_np.interaction_loop.session_types import SolverParameters
from transformers import AutoModelForCausalLM, AutoTokenizer
from pathlib import Path
from dataclasses import asdict, dataclass
from typing import Any
import time
import dbm
import argparse
import json
import random
import numpy as np

import os
os.environ["CUDA_VISIBLE_DEVICES"] = "2"


@dataclass
class PlanningBenchmarkResult:
    baseline_s: None | float  # None if planning failed
    llm_s: None | float  # None if planning failed
    nn_s: None | float  # None if planning failed


def benchmark_problem(
    problem: SolvedPlanningProblem,
    model: AutoModelForCausalLM,
    st_model: str,  # SentenceTransformers model
    tokenizer: AutoTokenizer,
    solver_parameters: SolverParameters,
    nn_database_file: Path,
    sub_step_s: float,
    seed: str,
    device: str,
) -> PlanningBenchmarkResult:
    _random = random.Random(seed)

    predicate = problem.predicate
    map_spec = problem.map_spec
    current_state = problem.state
    plan_len = get_max_plan_length(predicate, min_length=10)
    plan_shape: tuple[int, int] = tuple(np.array(problem.plan).shape)

    # Run baseline solver
    _start = time.time()
    _plan = get_plan(
        predicate=predicate,
        map_spec=map_spec,
        simulation_state=current_state,
        plan_len=plan_len,
        max_iter_n=solver_parameters.solver_max_iter_n,
        max_workers=solver_parameters.max_workers,
        sub_step_s=sub_step_s,
        options=planner_options,
        seed=str(_random.random()),
        verbose=False,
    )
    baseline_s = time.time() - _start
    if not _plan.is_sat:
        baseline_s = None

    # Run LLM
    _start = time.time()
    initial_candidate = get_model_plan(
        instruction=problem.instruction,
        state=problem.state,
        predicate=predicate,
        map_spec=map_spec,
        plan_shape=plan_shape,
        model=model,
        tokenizer=tokenizer,
        device=device,
        seed=str(_random.random()),
    )
    _plan = get_plan(
        predicate=predicate,
        map_spec=map_spec,
        simulation_state=current_state,
        plan_len=plan_len,
        max_iter_n=solver_parameters.solver_max_iter_n,
        max_workers=solver_parameters.max_workers,
        sub_step_s=sub_step_s,
        options=planner_options,
        seed=str(_random.random()),
        initial_candidate=initial_candidate,
        verbose=False,
    )
    llm_s = time.time() - _start
    if not _plan.is_sat:
        llm_s = None

    # Run closest neighbor
    # Nearest neighbor time is free because in practice embeddings
    # of the database will be cached
    nearest_solution = get_nearest_solution(
        instruction=problem.instruction,
        state=problem.state,
        predicate=predicate,
        map_spec=map_spec,
        plan_shape=plan_shape,
        solution_database_file=nn_database_file,
        model=st_model,
        device=device,
    )
    _start = time.time()
    initial_candidate = [
        list(li)
        for li in nearest_solution.plan
    ]
    _plan = get_plan(
        predicate=predicate,
        map_spec=map_spec,
        simulation_state=current_state,
        plan_len=plan_len,
        max_iter_n=solver_parameters.solver_max_iter_n,
        max_workers=solver_parameters.max_workers,
        sub_step_s=sub_step_s,
        options=planner_options,
        seed=str(_random.random()),
        initial_candidate=initial_candidate,
        verbose=False,
    )
    nn_s = time.time() - _start
    if not _plan.is_sat:
        nn_s = None

    # Assemble results
    results = PlanningBenchmarkResult(
        baseline_s=baseline_s,
        llm_s=llm_s,
        nn_s=nn_s,
    )
    return results


def main(
    database_file: Path,
    nn_database_file: Path,
    output_dir: Path,
    model_dir: Path,
    device: str,
    sub_step_s: float,
    test_n: int,
    st_model: str,
    seed: str,
):
    """`output_dir` is assumed to exist."""
    _random = random.Random(seed)

    # Load model
    tokenizer = AutoTokenizer.from_pretrained(model_dir)
    model = AutoModelForCausalLM.from_pretrained(model_dir).to(device)

    # Choose a random collection of problems to benchmark on
    with dbm.open(str(database_file.resolve()), "r") as db:
        keys = list(db.keys())
    test_keys = _random.sample(keys, k=test_n)

    # Solve every planning problem in the database
    results = list[dict[str, Any]]()
    for i, key in enumerate(test_keys):
        problem = get_solved_planning_problem(key, database_file)
        problem_results = benchmark_problem(
            problem=problem,
            model=model,
            device=device,
            solver_parameters=solver_parameters,
            sub_step_s=sub_step_s,
            seed=str(_random.random()),
            tokenizer=tokenizer,
            nn_database_file=nn_database_file,
            st_model=st_model,
        )
        results.append(asdict(problem_results))

        print(f"Test {i+1}/{len(test_keys)} done.")
        print(f"Results: {problem_results}")

    # Write results
    results_path = output_dir/"results.json"
    with open(results_path, "wt") as fp:
        json.dump(results, fp, indent=2,)
    print(f"Wrote {results_path}")

    # Plot results
    plot_dir = output_dir/"plots"
    plot_dir.mkdir()
    plot_results(results_path, output_dir=plot_dir)


if __name__ == "__main__":
    parser = argparse.ArgumentParser(
        prog="Benchmark the LLM-biased numerical planner.",
        description=('Use a fine-tuned LLM to kickstart anumerical planner.'),
    )
    _ = parser.add_argument(
        '--model_dir',
        type=Path,
        required=True,
        help='Directory with the fine-tuned weights.',
    )
    _ = parser.add_argument(
        '--test_database_file',
        type=Path,
        required=True,
        help='Test database file. Will be created if it does not exist.',
    )
    _ = parser.add_argument(
        '--nn_database_file',
        type=Path,
        required=True,
        help='Training database file for nearest neighbors.',
    )
    _ = parser.add_argument(
        '--output_dir',
        type=Path,
        required=True,
        help='Non-existent directory where output files will be written.',
    )
    _ = parser.add_argument(
        '--device',
        type=str,
        required=True,
        help='CUDA device.',
    )
    _ = parser.add_argument(
        '--session_parameters_json',
        type=Path,
        required=True,
        help='Solver parameters JSON file.',
    )
    _ = parser.add_argument(
        '--test_n',
        type=int,
        required=True,
        help='Number of test cases.',
    )
    _ = parser.add_argument(
        '--st_model',
        type=str,
        required=True,
        help='SentenceTransformers model for embeddings.',
    )
    _ = parser.add_argument(
        '--seed',
        type=str,
        required=True,
        help='Random seed.',
    )
    args = parser.parse_args()

    assert not args.output_dir.exists(), "ERROR: Output dir must not exist!"
    args.output_dir.mkdir()

    # Parse solver parameters JSON
    with open(args.session_parameters_json, "rt") as fp:
        session_parameters = json.load(fp)
    _solver_parameters = session_parameters["solver_parameters"]
    planner_options = PlannerOptions(
        **_solver_parameters["planner_options"]
    )
    solver_parameters = SolverParameters(
        solver_max_iter_n=_solver_parameters["solver_max_iter_n"],
        sub_step_s=_solver_parameters["sub_step_s"],
        max_workers=_solver_parameters["max_workers"],
        llm_attempt_n=_solver_parameters["llm_attempt_n"],
        planner_options=planner_options,
    )

    main(
        model_dir=args.model_dir,
        database_file=args.test_database_file,
        nn_database_file=args.nn_database_file,
        output_dir=args.output_dir,
        device=args.device,
        seed=args.seed,
        test_n=args.test_n,
        sub_step_s=solver_parameters.sub_step_s,
        st_model=args.st_model,
    )
