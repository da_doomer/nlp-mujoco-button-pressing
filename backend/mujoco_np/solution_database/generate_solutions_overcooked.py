"""Generate a file with solved planning problems for a given
sequence of overcooked maps.

The planning problems are generated by executing a simple hand-coded
agent that acts in an overcooked way.
"""
from ant_crafter.map_parser import get_map_spec
from ant_crafter.mujoco.save_video import save_video
from ant_crafter.predicates import ItemAtLocation, ItemInContactWithDiningTable, ItemInContactWithFryingPan, ItemInContactWithPlateDispenser, Predicate
from ant_crafter.predicates import Eventually
from ant_crafter.predicates import Always
from ant_crafter.predicates import RobotContactWithItem
from ant_crafter.predicates import ItemInContactWithItem
from ant_crafter.predicates import And
from ant_crafter.predicate_length import get_max_plan_length
from ant_crafter.planning import PlannerOptions, get_plan
from ant_crafter.mujoco.simulation import get_abstract_map_state, get_simulation_instance
from ant_crafter.robustness import get_robustness_value
from ant_crafter.task_sampling import Task
from mujoco_np.interaction_loop.session_types import SolverParameters
from mujoco_np.solution_database.solutions import SolvedPlanningProblem
from mujoco_np.solution_database.solutions import save_solved_planning_problem
from mujoco_np.solution_database.nearest_neighbor import DatabaseEmpty, get_nearest_solution
from mujoco_np.natural_programs.types import NaturalLanguageString
from mujoco_np.natural_programs.solver import get_ant_is_upright_predicate
from pathlib import Path
import argparse
import json
import random
import dbm
import traceback
import time
import numpy as np


def collect_task_data(
    map_path: Path,
    database_file: Path,
    planner_options: PlannerOptions,
    sub_step_s: float,
    video_dir: Path,
    st_model: str,
    device: str,
    verbose: bool,
    seed: str,
):
    _random = random.Random(seed)

    # Deserialize map
    map_spec = get_map_spec(map_path)

    # Heuristic plan schema:
    # 1. go to an object that can be cooked
    # 2. take the object to a frying pan
    # 3. take the cooked object to a bowl
    # TODO: programaticaly determine objects that can be taken to the frying
    # pan. For now, hardcoding for the crammed apple map
    cookable_items = [map_spec.items[0]]

    tasks = list[tuple[str, Predicate]]()
    time_per_instruction_s = 3
    steps_per_instruction = int(time_per_instruction_s/sub_step_s)
    # TODO: parametrize horizon len
    horizon_len_s = 2
    horizon_len = int(horizon_len_s/sub_step_s)
    for item in cookable_items:
        ### STEP 1: take the apple to the frying pan
        #contact_instruction = (
        #    f"make contact with {item.item_name}"
        #)
        #contact_predicate = Eventually(
        #    min_t=0,
        #    max_t=10,
        #    predicate=RobotContactWithItem(item_name=item.item_name),
        #)
        #tasks.append((contact_instruction, contact_predicate))

        frying_instruction = (
            f"take {item.item_name} to frying pan"
        )
        frying_predicate = Eventually(
            min_t=0,
            max_t=steps_per_instruction,
            predicate=ItemInContactWithFryingPan(
                item_name=item.item_name,
            ),
        )
        tasks.append((frying_instruction, frying_predicate))

        ### DEBUG: solve simultaneously
        #instruction = frying_instruction
        #predicate = frying_predicate
        #tasks.append((instruction, predicate))

        ### STEP 1.2: go to the pan position
        item = "apple_green"
        contact_instruction = (
            f"make contact with {item}"
        )
        contact_predicate = Eventually(
            min_t=0,
            max_t=steps_per_instruction,
            predicate=RobotContactWithItem(item_name=item),
        )
        tasks.append((contact_instruction, contact_predicate))

        ### STEP 2: Take the pan to the plate dispenser
        bowling_instruction = (
            f"take {item} to plate dispenser"
        )
        bowling_predicate = Eventually(
            min_t=0,
            max_t=steps_per_instruction,
            predicate=ItemInContactWithPlateDispenser(item_name=item),
        )
        tasks.append((bowling_instruction, bowling_predicate))

        ### DEBUG: also solve simultaneously
        #instruction = f"{contact_instruction} and {bowling_instruction}"
        #predicate = And(contact_predicate, bowling_predicate)
        #tasks.append((instruction, predicate))

        ### STEP 3.1 go to new item
        item = "apple_blue"
        contact_instruction = (
            f"make contact with {item}"
        )
        contact_predicate = Eventually(
            min_t=0,
            max_t=steps_per_instruction,
            predicate=RobotContactWithItem(item_name=item),
        )
        tasks.append((contact_instruction, contact_predicate))

        ### STEP 3.2: take bowl to the dining table
        #contact_instruction = (
        #    f"make contact with {bowl}"
        #)
        #contact_predicate = Eventually(
        #    min_t=0,
        #    max_t=steps_per_instruction,
        #    predicate=RobotContactWithItem(item_name=bowl),
        #)

        dining_instruction = (
            f"take {item} to dining table"
        )
        dining_predicate = Eventually(
            min_t=0,
            max_t=steps_per_instruction*2,
            predicate=ItemInContactWithDiningTable(item_name=item),
        )
        tasks.append((dining_instruction, dining_predicate))

        #### DEBUG: also solve simultaneously
        #instruction = f"{contact_instruction} and {dining_instruction}"
        #predicate = And(contact_predicate, dining_predicate)
        #tasks.append((instruction, predicate))

    # Instantiate simulation
    sim = get_simulation_instance(
        map_spec=map_spec,
        animation_fps=60.0,
        sub_step_s=sub_step_s,
    )
    current_state = sim.state

    # Solve each task
    for (instruction, predicate) in tasks:
        plan_len = get_max_plan_length(predicate, min_length=10)
        upright = get_ant_is_upright_predicate(plan_len)
        predicate = And(predicate, upright)
        plan_shape: tuple[int, int] = (plan_len, map_spec.robot.actuator_n)

        print(f"Current robot state: {get_abstract_map_state(sim)}")

        # Use the solution to the most similar planning problem
        # as an initial guess
        try:
            nearest_solution, similarity = get_nearest_solution(
                instruction=instruction,
                state=current_state,
                predicate=predicate,
                map_spec=map_spec,
                plan_shape=plan_shape,
                solution_database_file=database_file,
                model=st_model,
                device=device,
            )
            if similarity > 0.95:
                initial_candidate = nearest_solution.plan
                if len(initial_candidate) < plan_len:
                    zeros = np.zeros(plan_shape).tolist()
                    for i in range(len(initial_candidate)):
                        zeros[i] = initial_candidate[i]
                    initial_candidate = zeros
                if verbose:
                    lines = [
                        f"[collect_task_data] Using cached plan as initial candidate",
                        f"                 Solving:     NP(instruction='{instruction}', ...)",
                        f"                 Cached plan: NP(instruction='{nearest_solution.instruction}', ...)",
                    ]
                    print("\n".join(lines))
            else:
                initial_candidate = None
        except DatabaseEmpty:
            initial_candidate = None

        # Solve the planning problem
        _plan = get_plan(
            predicate=predicate,
            map_spec=map_spec,
            simulation_state=current_state,
            plan_len=plan_len,
            max_iter_n=solver_parameters.solver_max_iter_n,
            max_workers=solver_parameters.max_workers,
            sub_step_s=sub_step_s,
            options=planner_options,
            seed=str(_random.random()),
            initial_candidate=initial_candidate,
            #horizon_len=horizon_len,  # TODO: parametrize
            verbose=True,
        )

        ## DEBUGGING
        r = get_robustness_value(
            plan=_plan.plan,
            map_spec=map_spec,
            simulation_state=current_state,
            predicate=predicate,
            t=0,
            sub_step_s=sub_step_s,
        )
        if _plan.is_sat:
            assert r > 0, f"{r} is not sat!"
        ## /DEBUGGING

        # Queue video generation

        ### DEBUG
        # TODO: for some reason recreating the simulation
        # is necessary to obtain the same behaviour as in the optimization.
        # This means that the simulation state is not being serialized
        # correctly.
        # I've searched online and I think I'm doing it correctly, but
        # the issue persists.
        #
        # More info:
        # https://github.com/google-deepmind/dm_control/issues/64
        # https://github.com/google-deepmind/dm_control/issues/187
        # https://roboti.us/forum/index.php?threads/capturing-a-state-in-simulation.3956/
        # https://github.com/google-deepmind/dm_control/issues/187
        sim = get_simulation_instance(
            map_spec=map_spec,
            animation_fps=60.0,
            sub_step_s=sub_step_s,
        )
        ### /DEBUG

        sim.state = current_state
        sim.clear_video()
        for step in _plan.plan:
            sim.step(np.array(step))

        print(f"Robot state after optimization: {get_abstract_map_state(sim)}")

        if not _plan.is_sat:
            key = f"unsat_{time.time()}"
            video_output_path = video_dir/f"{key}.mp4"
            sim.save_video(video_output_path)
            print(f"Wrote {video_output_path}")
            return

        # Save the solved problem
        solution = SolvedPlanningProblem(
            instruction=NaturalLanguageString(instruction),
            predicate=predicate,
            state=current_state,
            map_spec=map_spec,
            plan=_plan.plan,
        )
        key = save_solved_planning_problem(solution, database_file)
        print(f"Stored solution to: '{instruction}'")

        # Save video
        video_output_path = video_dir/f"{str(key, 'utf8')}.mp4"
        sim.save_video(video_output_path)
        print(f"Wrote {video_output_path}")

        # Step simulation
        current_state = sim.state


def main(
    map_dir: Path,
    solver_parameters: SolverParameters,
    database_file: Path,
    video_dir: Path,
    st_model: str,
    device: str,
    seed: str,
):
    # If the databse doesn't exist, create an empty database
    with dbm.open(str(database_file.resolve()), "c") as db:
        pass

    _random = random.Random(seed)
    video_dir.mkdir(exist_ok=True)
    map_dirs_json = map_dir/"task_dirs.json"
    with open(map_dirs_json, "rt") as fp:
        _task_dirs: list[str] = json.load(fp)
    map_paths = [
        map_dir/tdir/"task.json"
        for tdir in _task_dirs
    ]

    for map_path in map_paths:
        print(map_path)
        collect_task_data(
            map_path=map_path,
            database_file=database_file,
            planner_options=solver_parameters.planner_options,
            sub_step_s=solver_parameters.sub_step_s,
            video_dir=video_dir,
            st_model=st_model,
            device=device,
            verbose=True,
            seed=str(_random.random()),
        )


if __name__ == "__main__":
    parser = argparse.ArgumentParser(
        prog="Collect solutions data.",
        description=(
            'Run a naive high-level policy to collect data ' +
            'from the low-level planner.'
        ),
    )
    _ = parser.add_argument(
        '--map_dir',
        type=Path,
        required=True,
        help='Directory with map sub-directories.',
    )
    _ = parser.add_argument(
        '--session_parameters_json',
        type=Path,
        required=True,
        help='Solver parameters JSON file.',
    )
    _ = parser.add_argument(
        '--database_file',
        type=Path,
        required=True,
        help='Database file. Will be created if it does not exist.',
    )
    _ = parser.add_argument(
        '--video_dir',
        type=Path,
        required=True,
        help='Directory to save videos to. Will be created if it does not exist.',
    )
    _ = parser.add_argument(
        '--st_model',
        type=str,
        required=True,
        help="""
        SentenceTransformers model for nearest neighbors lookups.
        It must match the model used to populate the nearest neighbors
        database.
        """,
    )
    _ = parser.add_argument(
        '--device',
        type=str,
        required=True,
        help="CUDA device for nearest neighbors lookups.",
    )
    _ = parser.add_argument(
        '--seed',
        type=str,
        required=True,
        help='Random seed.',
    )
    args = parser.parse_args()

    # Parse solver parameters JSON
    with open(args.session_parameters_json, "rt") as fp:
        session_parameters = json.load(fp)
    _solver_parameters = session_parameters["solver_parameters"]
    planner_options = PlannerOptions(
        **_solver_parameters["planner_options"]
    )
    solver_parameters = SolverParameters(
        solver_max_iter_n=_solver_parameters["solver_max_iter_n"],
        sub_step_s=_solver_parameters["sub_step_s"],
        max_workers=_solver_parameters["max_workers"],
        llm_attempt_n=_solver_parameters["llm_attempt_n"],
        planner_options=planner_options,
    )

    main(
        map_dir=args.map_dir,
        database_file=args.database_file,
        solver_parameters=solver_parameters,
        video_dir=args.video_dir,
        st_model=args.st_model,
        device=args.device,
        seed=args.seed,
    )
