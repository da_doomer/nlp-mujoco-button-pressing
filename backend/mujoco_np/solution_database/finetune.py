"""Finetune an LLM using a solution database."""
from ant_crafter.mujoco.simulation import SimulationState, get_abstract_map_state
from ant_crafter.mujoco.simulation import get_simulation_instance
from ant_crafter.predicates import Predicate
from ant_crafter.mujoco.map import MapSpec
from transformers import AutoModelForCausalLM, AutoTokenizer
from transformers import LogitsProcessorList
from transformers import StoppingCriteria, StoppingCriteriaList
from trl import SFTConfig, SFTTrainer
from pathlib import Path
from datasets import Dataset
from mujoco_np.solution_database.solutions import deserialize_planning_problem
from mujoco_np.solution_database.solutions import get_solved_planning_problem
from mujoco_np.solution_database.solutions import SolvedPlanningProblem
from dataclasses import asdict
from functools import cache
import json
import argparse
import dbm
import random
import numpy as np
from typing import TypeVar
import os
import torch


# https://github.com/huggingface/trl/issues/2338
os.environ["CUDA_VISIBLE_DEVICES"] = "2"
os.environ["TOKENIZERS_PARALLELISM"] = "false"


A = TypeVar("A")
ROUND_NDIGITS = 2


JSONType = (
    str
    | float
    | list["JSONType"]
    | list["JSONType"]
    | dict[str, "JSONType"]
)


def recursive_round(x: JSONType, ndigits: int) -> JSONType:
    if isinstance(x, str):
        return x
    if isinstance(x, int):
        return x
    if isinstance(x, float):
        return round(x, ndigits=ndigits)
    if isinstance(x, dict):
        new_x = dict[str, JSONType]()
        for k, v in x.items():
            new_x[k] = recursive_round(v, ndigits=ndigits)
        return new_x
    if isinstance(x, (list, tuple)):
        new_x = [
            recursive_round(xi, ndigits=ndigits)
            for xi in x
        ]
        return new_x
    raise ValueError(f"Type {type(x)} not supported!")


cache = dict[str, str]()

def get_pretty_problem(
    instruction: str,
    state: SimulationState,
    predicate: Predicate,
    map_spec: MapSpec,
    plan_shape: tuple[int, ...],
) -> str:
    predicate_str = str(predicate)
    state_str = str(state)
    map_spec_str = str(map_spec)

    # Since instantiating the simulation is expensive, we cache the
    # result
    key = ",".join([
        instruction,
        state_str,
        predicate_str,
        map_spec_str,
        str(plan_shape),
    ])
    if key in cache.keys():
        return cache[key]

    # Get a summary of the abstract state
    sim = get_simulation_instance(
        map_spec=map_spec,
        animation_fps=None,
        sub_step_s=1/10,  # Doesn't matter the value because we will not step
    )
    sim.state = state
    astate = get_abstract_map_state(sim)
    astate_summary = dict(
        robot_position=asdict(astate.robot_position),
        robot_yaw_radians=astate.robot_yaw_radians,
        robot_pitch_radians=astate.robot_pitch_radians,
        robot_roll_radians=astate.robot_roll_radians,
        #items=[asdict(i) for i in astate.items],
    )

    # Get a summary of the map spec (only including information not in the
    # abstract state summary)
    map_spec_dict = asdict(map_spec)
    map_spec_summary = dict(
        crafting_tables=map_spec_dict["crafting_tables"],
        walls=map_spec_dict["walls"],
        dining_tables=map_spec_dict["dining_tables"],
        trashcans=map_spec_dict["trashcans"],
        doors=map_spec_dict["doors"],
        buttons=map_spec_dict["buttons"],
    )

    # Encode as JSON
    output_format = f"List of lists of floating point numbers of shape {str(plan_shape)}, each between -1 and 1"
    json_problem = dict(
        state=astate_summary,
        # Since map spec in my debug maps is constant, I omit it for now
        # TODO: don't omit map
        # map_spec=map_spec_summary,
        instruction=instruction,
        #predicate=predicate_str,
        #output_format=output_format,
    )

    # Round the numbers in the JSON
    json_problem = recursive_round(json_problem, ndigits=ROUND_NDIGITS)
    result = json.dumps(
        json_problem,
        indent=2,
    )
    cache[key] = result
    return result


def get_pretty_plan(solution: SolvedPlanningProblem) -> str:
    discretized_plan = [
        [
            round(xi, 2)
            for xi in x
        ]
        for x in solution.plan
    ]
    return json.dumps(discretized_plan)


def get_pretty_str(solution: SolvedPlanningProblem) -> str:
    problem = get_pretty_problem(
        instruction=solution.instruction,
        state=solution.state,
        predicate=solution.predicate,
        map_spec=solution.map_spec,
        plan_shape=tuple(np.array(solution.plan).shape),
    )
    plan = get_pretty_plan(solution)
    return f"PROBLEM:\n{problem}\nOUTPUT:\n{plan}"


def get_train_test_split(
    l: list[A],
    test_size: float,
    seed: str,
) -> tuple[list[A], list[A]]:
    _random = random.Random(seed)
    test = list[A]()
    train = list[A]()
    for element in l:
        in_test = _random.random() < test_size
        if in_test:
            test.append(element)
        else:
            train.append(element)
    return train, test


def get_database_dataset(
    database_file: Path,
    keys: list[bytes],
) -> Dataset:
    # Retrieve all stored solutions
    with dbm.open(str(database_file.resolve()), "r") as db:
        serialized_solutions = [
            db.get(key)
            for key in keys
        ]
    solutions = [
        deserialize_planning_problem(serialized)
        for serialized in serialized_solutions
    ]

    # Format solutions
    texts = [
        get_pretty_str(solution)
        for solution in solutions
    ]
    dataset = Dataset.from_dict({
        "text": texts,
    })
    return dataset


def get_model_plan(
    instruction: str,
    state: SimulationState,
    predicate: Predicate,
    map_spec: MapSpec,
    plan_shape: tuple[int, int],
    model: AutoModelForCausalLM,
    tokenizer: AutoTokenizer,
    device: str,
    seed: str,
) -> list[list[float]]:
    """Return an array of shape `plan_shape` with numbers between -1 and 1."""
    _random = random.Random(seed)

    # Generate forcing the output to be a JSON array of floating point numbers
    # of correct shape.
    # We do this by scaffolding the generation: we only ask the
    # model to generate
    array_so_far = list[list[float]]()

    # Function to turn the current arrays and list into a partial array string
    def get_partial_output_str(
        array_so_far: list[list[float]],
        current_list: list[float],
    ) -> str:
        complete_list_strs = [
            f"[{','.join(map(str, li))}]"
            for li in array_so_far
        ]
        array_so_far_str = ",".join(complete_list_strs)
        if len(current_list) > 0:
            current_list_str = f"[{','.join(map(str, current_list))},"
        else:
            current_list_str = "["
        if len(array_so_far_str) > 0:
            inner_str = ','.join([array_so_far_str, current_list_str])
        else:
            inner_str = current_list_str
        string = f"[{inner_str}"
        return string

    # Constraint generation to floating point numbers
    def prefix_allowed_tokens_fn(batch_id, input_ids):
        allowed_tokens = [".", ",", "]"] + [str(i) for i in range(10)]
        return [
            tokenizer.convert_tokens_to_ids(token)
            for token in allowed_tokens
        ]

    # Form current prompt
    for i in range(plan_shape[0]):
        current_list = list[float]()

        for j in range(plan_shape[1]):
            problem = get_pretty_problem(
                instruction=instruction,
                state=state,
                predicate=predicate,
                map_spec=map_spec,
                plan_shape=plan_shape,
            )
            partial_output_str = get_partial_output_str(
                array_so_far,
                current_list,
            )
            prompt = f"PROBLEM:\n{problem}\nOUTPUT:\n{partial_output_str}"
            _inputs = tokenizer.encode(
                prompt,
                return_tensors="pt"
            ).to(device)

            # Sample model for next floating point number
            # TODO: what should the attention mask be
            # attention_mask = inputs["attention_mask"]
            max_length = 7  # 7 digit number is reasonable
            _outputs = model.generate(
                _inputs,
                max_new_tokens=max_length,
                # attention_mask=attention_mask,
                pad_token_id=tokenizer.eos_token_id,
                num_beams=10,
                do_sample=True,
                stop_strings=[",", "]"],
                prefix_allowed_tokens_fn=prefix_allowed_tokens_fn,
                tokenizer=tokenizer,
                #seed=[
                #    _random.randint(-2**16, 2**16),
                #    _random.randint(2**16, 2**16)
                #],
            )
            prompt_length = _inputs.shape[1]
            output = tokenizer.decode(
                _outputs[0][prompt_length:],
                skip_special_tokens=True,
            )

            # Parse number
            output = ''.join([
                c
                for c in output
                if c.isdigit() or c == '.'
            ])
            try:
                number = float(output)
                if number < -1.0 or number > 1.0:
                    raise ValueError("LLM returned weird stuff!")
            except ValueError:
                number = round(_random.random()*2-1, ROUND_NDIGITS)
            current_list.append(number)
        array_so_far.append(current_list)

    return array_so_far


def main(
    database_file: Path,
    output_dir: Path,
    test_size: float,  # [0, 1]
    epoch_n: int,
    device: str,
):
    checkpoint = "HuggingFaceTB/SmolLM-135M"

    # Define tokenizer
    tokenizer = AutoTokenizer.from_pretrained(
        checkpoint,
    )

    # Get dataset
    with dbm.open(str(database_file.resolve()), "r") as db:
        keys: list[bytes] = list(db.keys())
    train_keys, test_keys = get_train_test_split(
        l=keys,
        test_size=test_size,
        seed="69",
    )
    train_ds = get_database_dataset(
        database_file,
        train_keys,
    ).shuffle(seed=42)

    # Get length of longest sequence in the dataset
    lengths = list[int]()
    for example in train_ds:
        tokens = tokenizer.encode(example["text"])
        length = len(tokens)
        lengths.append(length)
    max_length = max(lengths)

    # Load model
    # for multiple GPUs install accelerate and do `model = AutoModelForCausalLM.from_pretrained(checkpoint, device_map="auto")`
    model = AutoModelForCausalLM.from_pretrained(
        checkpoint,
    ).to(device)

    for _ in range(epoch_n):
        epoch_dir = output_dir/f"epoch_{_+1}"
        training_args = SFTConfig(
            output_dir=str(epoch_dir.resolve()),
            max_seq_length=max_length,
        )
        # Run training loop
        trainer = SFTTrainer(
            model,
            train_dataset=train_ds,
            args=training_args,
        )
        trainer.train()

    # Test
    _random = random.Random("asd")
    for i, test_key in enumerate(test_keys):
        test_case = get_solved_planning_problem(
            key=test_key,
            database_file=database_file,
        )
        output = get_model_plan(
            instruction=test_case.instruction,
            state=test_case.state,
            predicate=test_case.predicate,
            map_spec=test_case.map_spec,
            plan_shape=tuple(np.array(test_case.plan).shape),
            model=model,
            tokenizer=tokenizer,
            device=device,
            seed=str(_random.random()),
        )
        target_output = get_pretty_plan(test_case)
        output_log = dict(
            output=output,
            target_output=target_output,
            planning_problem=str(test_case),
        )
        filename = f"example_output_{i}.json"
        with open(output_dir/filename, "wt") as fp:
            json.dump(
                output_log,
                fp,
                indent=2,
            )
        print(f"Wrote {output_dir/filename}")
        print(f"Done {i+1}/{len(test_keys)}")


if __name__ == "__main__":
    parser = argparse.ArgumentParser(
        prog="Finetune LLM using solution database.",
        description=('Finetune an LLM using a solution database.'),
    )
    _ = parser.add_argument(
        '--database_file',
        type=Path,
        required=True,
        help='Database file.',
    )
    _ = parser.add_argument(
        '--epoch_n',
        type=int,
        required=True,
        help='Number of epochs.',
    )
    _ = parser.add_argument(
        '--test_size',
        type=float,
        required=True,
        help='Fraction of the dataset used to Number between 0 and 1.',
    )
    _ = parser.add_argument(
        '--output_dir',
        type=Path,
        required=True,
        help='Non-existent output directory.',
    )
    _ = parser.add_argument(
        '--device',
        type=str,
        required=True,
        help='CUDA device.',
    )
    args = parser.parse_args()

    assert args.database_file.exists(), "Error: Database file doesn't exist!"
    assert not args.output_dir.exists(), "Error: Output directory exists!"

    main(
        database_file=args.database_file,
        output_dir=args.output_dir,
        test_size=args.test_size,
        epoch_n=args.epoch_n,
        device=args.device,
    )
