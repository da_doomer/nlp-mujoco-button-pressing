"""Type definitions for solutions to planning problems."""
from ant_crafter.mujoco.map import MapSpec
from ant_crafter.predicates import Predicate
from mujoco_np.natural_programs.types import NaturalLanguageString
from ant_crafter.mujoco.simulation import SimulationState
from dataclasses import dataclass
from pathlib import Path
import dbm
import jsonpickle


@dataclass(frozen=True)
class SolvedPlanningProblem:
    instruction: NaturalLanguageString
    predicate: Predicate
    state: SimulationState
    map_spec: MapSpec
    plan: list[list[float, ...], ...]
    # TODO: add sub_step_s


def serialize_planning_problem(solution: SolvedPlanningProblem) -> str:
    serialized: str = jsonpickle.encode(
        solution,
        make_refs=False,
        warn=True,
    )
    return serialized


def deserialize_planning_problem(serialized: str) -> SolvedPlanningProblem:
    solution: SolvedPlanningProblem = jsonpickle.loads(
        serialized,
        on_missing='error',
    )
    return solution


def save_solved_planning_problem(
    solution: SolvedPlanningProblem,
    database_file: Path,
) -> bytes:
    """Returns the database ID."""
    with dbm.open(str(database_file.resolve()), "c") as db:
        key = bytes(str(len(db.keys())), "utf8")
        serialized = serialize_planning_problem(solution)
        db[key] = bytes(serialized, "utf-8")
    return key


def get_solved_planning_problem(
    key: bytes,
    database_file: Path
) -> SolvedPlanningProblem:
    """Iterate through the planning problems in the database."""
    with dbm.open(str(database_file.resolve()), "r") as db:
        serialized = db[key]
        solution = deserialize_planning_problem(serialized)
    return solution
