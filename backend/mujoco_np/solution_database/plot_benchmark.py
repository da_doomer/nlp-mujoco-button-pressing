"""Plot results of a benchmark file."""
from pathlib import Path
import seaborn as sns
import matplotlib.pyplot as plt
import json
import argparse
import pandas as pd


def plot_success_times(results_file: Path, time_filename: Path) -> None:
    """`output_dir` is assumed to exist."""
    # Load results
    with open(results_file, "rt") as fp:
        _data = json.load(fp)
    entries = list()
    for datum in _data:
        entries.append(dict(
            method="planner",
            time=datum["baseline_s"],
        ))
        entries.append(dict(
            method="planner+llm",
            time=datum["llm_s"],
        ))
        entries.append(dict(
            method="planner+nn",
            time=datum["nn_s"],
        ))
    data = pd.DataFrame(entries)

    # Initialize the figure with a logarithmic x axis
    f, ax = plt.subplots(figsize=(7, 6))
    #ax.set_xscale("log")

    # Plot the orbital period with horizontal boxes
    sns.boxplot(
        data, x="time", y="method", hue="method",
        #whis=(0, 100), width=.6, palette="vlag",
        ax=ax,
    )

    # Add in points to show each observation
    sns.stripplot(data, x="time", y="method", size=4, color=".3", ax=ax)

    #ax.xaxis.grid(True)
    #ax.set(ylabel="")
    #sns.despine(trim=True, left=True)

    f.savefig(time_filename)


def plot_results(results_file: Path, output_dir: Path) -> None:
    time_filename = output_dir/"plot_time.png"
    plot_success_times(results_file=results_file, time_filename=time_filename)
    print(f"Wtote {time_filename}")


if __name__ == "__main__":
    parser = argparse.ArgumentParser(
        prog="Collect solutions data.",
        description=(
            'Run a naive high-level policy to collect data ' +
            'from the low-level planner.'
        ),
    )
    _ = parser.add_argument(
        '--results_path',
        type=Path,
        required=True,
        help='Path with benchmark data.',
    )
    _ = parser.add_argument(
        '--output_dir',
        type=Path,
        required=True,
        help='Non-existent output dir.',
    )
    args = parser.parse_args()

    assert not args.output_dir.exists(), "Output dir must not exist!"
    args.output_dir.mkdir()

    plot_results(
        results_file=args.results_path,
        output_dir=args.output_dir,
    )
