"""Get an initial plan candidate by choosing the nearest-neighbor problem
in a database.

Distance between problems is the euclidean distance between their
embeddings."""
from ant_crafter.mujoco.map import MapSpec
from ant_crafter.predicates import Predicate
from ant_crafter.mujoco.simulation import SimulationState
from sentence_transformers import SentenceTransformer
from mujoco_np.solution_database.finetune import get_pretty_problem
from mujoco_np.solution_database.solutions import SolvedPlanningProblem
from mujoco_np.solution_database.solutions import get_solved_planning_problem
from alive_progress import alive_it
import numpy as np
from pathlib import Path
import dbm
import json
import torch


embedding_cache_file = Path(__file__).parent/"embedding_database"


cache = dict[str, list[float]]()


class DatabaseEmpty(Exception):
    pass


def get_embedding(
    instruction: str,
    state: SimulationState,
    predicate: Predicate,
    map_spec: MapSpec,
    model: SentenceTransformer,
    plan_shape: tuple[int, int],
    device: str,
) -> torch.Tensor:
    """Get an embedding of the given problem."""
    prompt = get_pretty_problem(
        instruction=instruction,
        state=state,
        predicate=predicate,
        map_spec=map_spec,
        plan_shape=plan_shape,
    )

    key = json.dumps(dict(
        prompt=prompt,
        model=hash(model),
    ))

    if key in cache.keys():
        #print(f"[Nearest-neighbors] In-memory cache hit")
        return torch.tensor(cache[key]).to(device)

    with dbm.open(str(embedding_cache_file.resolve()), "c") as db:
        # If prompt was serialized, return cached response
        if key in db.keys():
            #print(f"[Nearest-neighbors] In-disk cache hit")
            embedding_str = db[key].decode("utf-8")
            embedding: list[float] = json.loads(embedding_str)
            cache[key] = embedding
            return torch.tensor(embedding).to(device)

    embedding = model.encode([prompt])[0].tolist()
    embedding_str = json.dumps(embedding)

    with dbm.open(str(embedding_cache_file.resolve()), "c") as db:
        db[key] = bytes(embedding_str, "utf-8")

    cache[key] = embedding
    return torch.tensor(embedding).to(device)


def get_nearest_solution(
    instruction: str,
    state: SimulationState,
    predicate: Predicate,
    map_spec: MapSpec,
    model: str,
    solution_database_file: Path,
    plan_shape: tuple[int, int],
    device: str,
) -> tuple[SolvedPlanningProblem, float]:
    _model = SentenceTransformer(model, device=device)

    # Deserialize all the solutions in the database
    with dbm.open(str(solution_database_file.resolve()), "r") as db:
        keys = list(db.keys())

    if len(keys) == 0:
        raise DatabaseEmpty()

    solutions = [
        get_solved_planning_problem(
            key=key,
            database_file=solution_database_file,
        )
        for key in keys
    ]

    # Get prompt for given problem
    problem_embedding = get_embedding(
        instruction=instruction,
        state=state,
        predicate=predicate,
        map_spec=map_spec,
        model=_model,
        plan_shape=plan_shape,
        device=device,
    )

    # Get prompt for each problem in database
    solution_embeddings = torch.stack([
        get_embedding(
            instruction=solution.instruction,
            state=solution.state,
            predicate=solution.predicate,
            map_spec=solution.map_spec,
            model=_model,
            plan_shape=tuple(np.array(solution.plan)),
            device=device,
        )
        for solution in solutions
    ])

    # Choose closest solution
    similarities = _model.similarity(
        problem_embedding,
        solution_embeddings,
    )[0]

    print(similarities)
    sim, i = torch.max(similarities, 0)
    closest_solution = solutions[i]

    # Return the plan of the closest
    return closest_solution, sim


def warmup_cache(
    model: str,
    device: str,
    solution_database_file: Path,
):
    """Get the database embeddings into memory."""
    _model = SentenceTransformer(model, device=device)

    # Deserialize all the solutions in the database
    with dbm.open(str(solution_database_file.resolve()), "r") as db:
        keys = list[bytes](db.keys())

    solutions = [
        get_solved_planning_problem(
            key=key,
            database_file=solution_database_file,
        )
        for key in keys
    ]

    # Get prompt for each problem in database
    print("Warming up nearest neighbor plan cache")
    for solution in alive_it(solutions):
        _ = get_embedding(
            instruction=solution.instruction,
            state=solution.state,
            predicate=solution.predicate,
            map_spec=solution.map_spec,
            model=_model,
            plan_shape=tuple(np.array(solution.plan)),
            device=device,
        )
    print("Done")
