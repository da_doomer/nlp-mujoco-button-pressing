# LLM finetuning

This directory contains files to finetune an LLM on plan data and benchmark
its effect on performance.

The LLM is trained on tuples of the form `(problem_spec, plan)`, where
- `problem_spec` is a text description of the state of the environment,
  a natural language instruction (e.g., "make the cube and the sphere collide")
  and a formalized representation of the instruction.
- `plan` is a sequence of floating-point vectors corresponding to a successful
  plan. Each entry in a vector is the low-level actuator signal for a joint
  in the robot.


First run `generate_solutions.py` to generate a database of such tuples.
Then run `finetune.py` to finetune an LLM on this data. Generate
another database to use as the baselines nearest-neighbors lookup table.
Finally, run `benchmark_finetuned_planner.py` to benchmark the finetuned LLM
against (1) baseline pure numerical solver, (2) nearest neighbors.

Both the LLM and the nearest neighbors baseline have an API of the form
`guess(problem_spec) -> candidate_plan`. The `candidate_plan` is used as the
initial guess of the baseline pure numerical solver, which iterates a
population-based search (CMA-ES) until a solution to the spec is found
or a timeout is reached.

The time-to-solution is measured and plotted for all three approaches.
