import argparse
import random
import dbm
from pathlib import Path
from mujoco_np.solution_database.solutions import get_solved_planning_problem
from mujoco_np.solution_database.finetune import get_pretty_str
from mujoco_np.solution_database.finetune import get_pretty_problem
import numpy as np


if __name__ == "__main__":
    parser = argparse.ArgumentParser(
        prog="Print example data.",
        description=(
            'Print an example datum from a solutions database.'
        ),
    )
    _ = parser.add_argument(
        '--database_file',
        type=Path,
        required=True,
        help='Database file. Will be created if it does not exist.',
    )
    args = parser.parse_args()

    database_file: Path = args.database_file
    assert database_file.exists()
    with dbm.open(str(database_file.resolve()), "r") as db:
        # Pick a key
        keys = list(db.keys())
    key: bytes = max(keys)

    # Retrieve the solution
    solution = get_solved_planning_problem(key, database_file)
    solution_str = get_pretty_str(solution)

    # Print solution
    print(solution_str)

    print(f"There are {len(keys)} entries in the database!")

    prompt = get_pretty_problem(
        instruction=solution.instruction,
        state=solution.state,
        predicate=solution.predicate,
        map_spec=solution.map_spec,
        plan_shape=tuple(np.array(solution.plan).shape),
    )
    print(f"Latest entry:")
    print(prompt)
