# natural-programming-mujoco

[Live demo](https://da_doomer.gitlab.io/nlp-mujoco-button-pressing/)

This project contains an implementation of the Natural Programming system to control a 3D robot in an overcooked-like environment, and a frontend to run user studies over said implementation.

## Backend

The backend contains:
- A Python implementation of the Natural Programming System.
- A Mujoco implementation of the simulated environment.
- Google Firebase utilities to coordinate the interaction loop with the frontend.

## Frontend

The frontend is a website built with SvelteKit that uses Google Firebase to coordinate the interaction loop with the backend.