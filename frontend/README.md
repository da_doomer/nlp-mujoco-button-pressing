# Frontend

## Requirements

Create a Google Firebase project if you have not. This Firebase project
is shared between the frontend and the backend.

### Firebase frontend config

Create file `frontend/src/lib/firebase_config.ts` with your firebase
configuration:


```typescript
// frontend/src/lib/firebase_config.ts
export const firebaseConfig = {
    //...
};
```

### Firebase CORS

Adjust CORS in Firebase so that data can be accessed from any domain.

From the [Firebase documentation](https://firebase.google.com/docs/storage/web/download-files#cors_configuration),
save this to `cors.json`:

```json
[
  {
    "origin": ["*"],
    "method": ["GET"],
    "maxAgeSeconds": 3600
  }
]
```

Then run `gsutil cors set cors.json gs://<your-cloud-storage-bucket`.

You need `gsutil` installed and initialized: [docs](https://cloud.google.com/storage/docs/gsutil_install).

# Development

Once Firebase is setup, you can run a live development build by running the
following:

```bash
npm run install
npm run dev
```

# Deployment

The frontend is a static web page, which can be easily deployed as any
other collection of HTML and Javascript documents. Build the project
by running `npm run build --max-old-space-size=4096`.
