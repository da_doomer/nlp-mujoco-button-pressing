#!/bin/bash

echo 'export const firebaseConfig = {'
echo "	apiKey: \"${firebaseApiKey}\","
echo '	authDomain: "np-mujoco.firebaseapp.com",'
echo '	projectId: "np-mujoco",'
echo '	storageBucket: "np-mujoco.appspot.com",'
echo '	messagingSenderId: "1041111061833",'
echo '	appId: "1:1041111061833:web:877d0e2ce9bca03b3e3698"'
echo '};'
