import { get_session_outcomes } from '$lib/firebase';
import type { PageLoad } from './$types';

export const load: PageLoad = () => {
	const session_outcomes_promise = get_session_outcomes();
	return {
		session_outcomes_promise: session_outcomes_promise,
	};
};
