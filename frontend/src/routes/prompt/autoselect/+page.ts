import { get_unanswered_prompt_id } from '$lib/firebase';
import type { PageLoad } from './$types';

export const load: PageLoad = () => {
	const prompt_id_promise = get_unanswered_prompt_id();
	return {
		prompt_id_promise: prompt_id_promise
	};
};
