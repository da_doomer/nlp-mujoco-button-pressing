import { error } from '@sveltejs/kit';
import type { PageLoad } from './$types';

export const load: PageLoad = ({ url }) => {
	const chain_id = url.searchParams.get("chain_id");
	if (chain_id === null) return error(404);
	return {
		chain_id: chain_id,
	};
};
