import { get_prompt } from '$lib/firebase';
import { error } from '@sveltejs/kit';
import type { PageLoad } from './$types';

export const load: PageLoad = ({ url }) => {
	const prompt_id = url.searchParams.get("prompt_id");
	if (prompt_id === null)
		return error(404);
	const prompt_promise = get_prompt(prompt_id);
	return {
		prompt_id: prompt_id,
		prompt_promise: prompt_promise
	};
};
