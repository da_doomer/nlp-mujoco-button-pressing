import { get_session_outcome } from '$lib/firebase';
import { error } from '@sveltejs/kit';
import type { PageLoad } from './$types';

export const load: PageLoad = ({ url }) => {
	const session_outcome_id = url.searchParams.get("session_outcome_id");
	if (session_outcome_id === null)
		return error(404);
	const session_outcome_promise = get_session_outcome(session_outcome_id);
	return {
		session_outcome_promise: session_outcome_promise,
	};
};
