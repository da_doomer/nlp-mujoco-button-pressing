/**
 * Firebase facade.
 **/
import { type Prompt } from './types';
import { type Image } from './types';
import { type Video } from './types';
import { type Response } from './types';
import { type SessionOutcome } from './types';
import { initializeApp } from "firebase/app";
import { getFirestore } from "firebase/firestore";
import { orderBy } from "firebase/firestore";
import { collection } from "firebase/firestore";
import { where } from "firebase/firestore";
import { query } from "firebase/firestore";
import { limit } from "firebase/firestore";
import { getDocs } from "firebase/firestore";
import { updateDoc } from "firebase/firestore";
import { doc } from "firebase/firestore"; 
import { getDoc } from "firebase/firestore"; 
import { addDoc } from "firebase/firestore"; 
import { getStorage } from "firebase/storage";
import { ref } from "firebase/storage";
import { getBlob } from "firebase/storage";
import { firebaseConfig } from './firebase_config';


const app = initializeApp(firebaseConfig);
const db = getFirestore(app);
const PROMPTS_COLLECTION = 'prompts';
const RESPONSES_COLLECTION = 'responses';
const SESSION_OUTCOMES_COLLECTION = "session_outcomes";

const NO_PREVIOUS_SESSION = "NO_PREVIOUS_SESSION";

/**
 * Retrieve the given storage blob from Firebase.
 **/
async function get_file(storage_blob_id: string): Promise<Blob> {
	const storage = getStorage();
	const pathReference = ref(storage, storage_blob_id);
	return getBlob(pathReference);
}


/**
 * Retrieve the given prompt from Firebase.
 **/
async function get_prompt(prompt_id: string): Promise<Prompt> {
	// Fetch raw data
	const docRef = doc(db, PROMPTS_COLLECTION, prompt_id);
	const docSnap = await getDoc(docRef);
	const raw_data = docSnap.data();

	if (raw_data === undefined)
		throw `Firebase returned no data for prompt ID ${prompt_id}!`;

	// Parse response and fetch additional data if needed
	const prompt_data: (string | Image | Video)[] = [];
	for (const datum of raw_data["data"]) {
		if (datum["type"] === "string") {
			prompt_data.push(datum["message"]);
		} else if (datum["type"] === "image") {
			const storage_blob_id = datum["message"];
 			const array_buffer = await get_file(storage_blob_id);
			const parsed_datum: Image = {
				type: "image",
				data: array_buffer,
			}
			prompt_data.push(parsed_datum);
		} else if (datum["type"] === "video") {
			const storage_blob_id = datum["message"];
 			const array_buffer = await get_file(storage_blob_id);
			const parsed_datum: Video = {
				type: "video",
				data: array_buffer,
			}
			prompt_data.push(parsed_datum);
		} else {
			throw `Failed to parsed prompt data from prompt_id '${prompt_id}'!`;
		}
	}
	const prompt = {
		data: prompt_data,
		has_response: raw_data["has_response"],
	};
	return prompt;
}


/**
 * Upload response.
 **/
async function upload_response(
	response: Response,
	prompt_id: string,
): Promise<undefined> {
	// Upload response
	await addDoc(
		collection(db, RESPONSES_COLLECTION),
		{
			data: response,
			prompt_id: prompt_id,
		},
	);

	// Update prompt
	const prompt_ref = doc(db, PROMPTS_COLLECTION, prompt_id);
	await updateDoc(prompt_ref, {
		has_response: true,
	});
}


/**
 * Get all session outcomes in the database.
 */
async function get_response(
	response_id: string
): Promise<Response> {
	const docRef = doc(db, RESPONSES_COLLECTION, response_id);
	const docSnap = await getDoc(docRef);
	const data = docSnap.data();
	if (data === undefined)
		throw `Firebase has not data for response ID ${response_id}!`;
	const response = data["data"];
	return response;
}


/**
 * Get the prompt ID of a prompt with no response.
 **/
async function get_unanswered_prompt_id(): Promise<string> {
	// Get prompts that have no response
	const prompts_ref = collection(db, PROMPTS_COLLECTION);
	const q = query(
		prompts_ref,
		where("has_response", "==", false),
		orderBy("timestamp"),
		limit(1),
	);

	// Choose the most recent one
	const querySnapshot = await getDocs(q);
	const docs = querySnapshot.docs;
	if (docs.length === 0)
		throw "There are no prompts without response!";
	const prompt_id = docs[0].id;
	return prompt_id;
}


/**
 * Get the prompt ID of a prompt with no response.
 **/
async function get_latest_unanswered_chain_prompt_id(
	chain_id: string,
): Promise<string> {
	// Get prompts that have no response
	const prompts_ref = collection(db, PROMPTS_COLLECTION);
	const q = query(
		prompts_ref,
		where("has_response", "==", false),
		where("chain_id", "==", chain_id),
		orderBy("timestamp"),
		limit(1),
	);

	// Choose the most recent one
	const querySnapshot = await getDocs(q);
	const docs = querySnapshot.docs;
	if (docs.length === 0)
		throw "There are no prompts without response!";
	const prompt_id = docs[0].id;
	return prompt_id;
}


/**
 * Get all session outcomes in the database.
 */
async function get_session_outcomes(): Promise<SessionOutcome[]> {
	const session_outcomes_ref = collection(db, SESSION_OUTCOMES_COLLECTION);
	const q = query(session_outcomes_ref);
	const querySnapshot = await getDocs(q);
	const docs = querySnapshot.docs;
	const session_outcomes = [];
	for (const doc of docs) {
		const data = doc.data();
		const session_outcome = {
			full_video_blob_id: data["full_video_blob_id"],
			interaction_prompt_id: data["interaction_prompt_id"],
			interaction_response_id: data["interaction_response_id"],
			previous_session_id: data["previous_session_id"],
			session_id: data["session_id"],
			timestamp: data["timestamp"],
			id: doc.id,
		};
		session_outcomes.push(session_outcome);
	}
	return session_outcomes;
}


/**
 * Get all session outcomes in the database.
 */
async function get_session_outcome(
	session_outcome_id: string
): Promise<SessionOutcome> {
	const docRef = doc(db, SESSION_OUTCOMES_COLLECTION, session_outcome_id);
	const docSnap = await getDoc(docRef);
	const data = docSnap.data();
	if (data === undefined)
		throw `Firebase has not data for session outcome ID ${session_outcome_id}!`;
	const session_outcome = {
		full_video_blob_id: data["full_video_blob_id"],
		interaction_prompt_id: data["interaction_prompt_id"],
		interaction_response_id: data["interaction_response_id"],
		previous_session_id: data["previous_session_id"],
		session_id: data["session_id"],
		timestamp: data["timestamp"],
		id: docSnap.id,
	};

	return session_outcome;
}


export {
	get_file,
	get_prompt,
	upload_response,
	get_response,
	get_unanswered_prompt_id,
	get_latest_unanswered_chain_prompt_id,
	get_session_outcomes,
	get_session_outcome,
	NO_PREVIOUS_SESSION,
};
