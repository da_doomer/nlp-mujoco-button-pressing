/**
 * Helper function to get URL for a blob. This is to embed video or image
 * blobs in a `<img>` or `<video>` component.
 */
function get_url(data: Blob) {
	const imageUrl = URL.createObjectURL(data);
	return imageUrl;
}

export {
	get_url,
};
