/**
 * Types for the frontend.
 **/
type Image = {
	type: "image",
	data: Blob,
};
type Video = {
	type: "video",
	data: Blob,
};

type Prompt = {
    data: (string | Image | Video)[],
};

type Response = string;

type SessionOutcome = {
	full_video_blob_id: string,
	interaction_prompt_id: string,
	interaction_response_id: string,
	previous_session_id: string,
	session_id: string,
	timestamp: number,
	id: string,
};

export type {
	Image,
	Video,
	Prompt,
	Response,
	SessionOutcome,
};
