/**
 * Utilities for interfacing with Mermaid JS.
 */
import { type SessionOutcome as SessionOutcomeType } from '$lib/types';
import { NO_PREVIOUS_SESSION } from './firebase';


/**
 * Get a Mermaid markup string for the graph induced by the given array of
 * session outcomes.
 */
function get_chain_mermaid(session_outcomes: SessionOutcomeType[]): string {
	// Construct markup line-by-line
	const lines = ["flowchart TB"];

	// Push all session IDs as nodes
	const session_ids = new Set<string>();
	for (const session_outcome of session_outcomes) {
		const session_id = session_outcome.session_id;
		session_ids .add(session_id);
	}
	for (const session_id of session_ids) {
		const line = `${session_id};`;
		lines.push(line);
	}

	// Push all session outcomes with a non-null previous session as nodes
	const session_outcome_ids = new Set<string>();
	for (const session_outcome of session_outcomes) {
		const parent = session_outcome.previous_session_id;
		if (parent === NO_PREVIOUS_SESSION)
			continue;
		const session_outcome_id = session_outcome.id;
		session_outcome_ids.add(session_outcome_id);
	}
	for (const session_outcome_id of session_outcome_ids) {
		// Make node have special shape
		const line = `${session_outcome_id}>${session_outcome_id}]`
		lines.push(line);

		// Make node clickable
		const url = `/session_outcome_visualization?session_outcome_id=${session_outcome_id}`;
		const interaction_line = `click ${session_outcome_id} "${url}"`;
		lines.push(interaction_line);
	}

	// Push every session outcome as a parent --> session outcome --> child
	// subgraph.
	for (const session_outcome of session_outcomes) {
		const parent = session_outcome.previous_session_id;
		const child = session_outcome.session_id;
		const arrow = session_outcome.id;

		if (parent === NO_PREVIOUS_SESSION)
			continue;

		const line1 = `${parent} --> ${arrow}`;
		const line2 = `${arrow} --> ${child}`;
		lines.push(line1);
		lines.push(line2);
	}

	const mermaid_markup = lines.join("\n");
	return mermaid_markup;
}

export {
	get_chain_mermaid
};
